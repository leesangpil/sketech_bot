using UnityEngine;
using System.Collections;

public class CaptureImage : MonoBehaviour {

    public static WebCamTexture webcam;
    private Texture2D snap_shot;

    private int Width;
    private int Height;

    public float ing_time;
    public static bool time_start;
    public static bool isClick;
    public static bool isCapture;         // ĸ�İ� ������ ��Ȳ����

    private float scrollSpeed;
    private float offset;

	// Use this for initialization
	void Start () {

        if (Application.HasUserAuthorization(UserAuthorization.WebCam | UserAuthorization.Microphone))
        {
            //print("��ķ ����");

            webcam = new WebCamTexture();
            webcam.Play();
        }

        Width = webcam.width;
        Height = webcam.height;

        snap_shot = new Texture2D(Width, Height);
        ing_time = 0;
        time_start = false;
        isClick = false;
        isCapture = true;
        scrollSpeed = 0.3f;
        offset = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {

        offset = Time.time * scrollSpeed;
        GameObject.Find("Background_Plane").renderer.material.mainTextureOffset = new Vector2(offset, 0);

        if (time_start == true && isClick == true && isCapture == true)
        {
            if (ing_time == 0.0f)
            GameObject.Find("AudioSource_OneTwoThree").GetComponent<AudioSource>().Play();
            if (ing_time > 3.0f)        // ��Ĭ !
            {
                ing_time = 0;
                time_start = false;
                isCapture = false;
                GameObject.Find("flash").GetComponent<Animation>().Play();
                GameObject.Find("AudioSource_Capture").GetComponent<AudioSource>().Play();
                TakeSnapShot(snap_shot);
                GameObject.Find("Capture_Place").renderer.material.mainTexture = snap_shot;
            }
            else
            {
                ing_time += Time.deltaTime;
            }
        }
        if (isClick == false)
        {
            GameObject.Find("Capture_Place").renderer.material.mainTexture = webcam;
        }
	}

    public void TakeSnapShot(Texture2D Snap_texture)
    {
        Snap_texture.SetPixels(webcam.GetPixels());
        Snap_texture.Apply();

        save_Texture2D(Snap_texture, "capture");
    }

    void save_Texture2D(Texture2D save_texture, string name)
    {
        byte[] _buffer = save_texture.EncodeToPNG();

        System.IO.FileStream _fs = new System.IO.FileStream(name + ".bmp", System.IO.FileMode.Create, System.IO.FileAccess.Write);
        System.IO.BinaryWriter _bw = new System.IO.BinaryWriter(_fs);
        _bw.Write(_buffer);
        _bw.Close();
        _fs.Close();
    }

    public static void OnDestroy()
    {
        webcam.Stop();
    }
}
