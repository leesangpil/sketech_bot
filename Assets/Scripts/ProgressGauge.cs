using UnityEngine;
using System.Collections;

public class ProgressGauge : MonoBehaviour {

	public GameObject cube;
    private Transform _myPos;

    public Texture red;
    public Texture blue;
    public Texture yellow;
    public Texture green;

    public Texture red2;
    public Texture blue2;
    public Texture yellow2;
    public Texture green2;

    private GameObject[] Cubes;

	// Use this for initialization
	void Start ()
    {
        Cubes = new GameObject[50];
        _myPos = gameObject.transform;
        GameObject tempCube;
        for(int i=0;i<50;i++)
        {
            tempCube = Instantiate(cube,new Vector3(0.295f*((float)i - 24.5f), -5, 0) ,new Quaternion(0, 0, 0, 0 )) as GameObject;
            tempCube.transform.Rotate(0, 90, 90);
            Cubes[i] = tempCube;
            Cubes[i].transform.parent = _myPos;
            if (i < 13)
            {
                Cubes[i].renderer.material.mainTexture = yellow2;
            }
            else if (i >= 13 && i < 25)
            {
                Cubes[i].renderer.material.mainTexture = green2;
            }
            else if (i >= 25 && i < 38)
            {
                Cubes[i].renderer.material.mainTexture = blue2;
            }
            else
            {
                Cubes[i].renderer.material.mainTexture = red2;
            }
        }
	}
	
	// Update is called once per frame
	void Update () {

        int gauge = (int)GameObject.Find("ProgramManager").GetComponent<RobotControl>().progress;
        
        for (int i = 0; i < 50; i++)
        {
            if (gauge >= 2 * i + 1)
            {
                if (i < 13)
                {
                    Cubes[i].renderer.material.mainTexture = yellow;
                }
                else if (i >= 13 && i < 25)
                {
                    Cubes[i].renderer.material.mainTexture = green;
                }
                else if (i >= 25 && i < 38)
                {
                    Cubes[i].renderer.material.mainTexture = blue;
                }
                else
                {
                    Cubes[i].renderer.material.mainTexture = red;
                }
            }
        }
	}
}
