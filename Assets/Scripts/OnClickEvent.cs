using UnityEngine;
using System.Collections;

public class OnClickEvent : MonoBehaviour {

    public Texture[] _texture;

    private bool _onbutton;
    private bool _clickbutton;
    private bool _onbutton1;
    private bool _clickbutton1;
    private bool _onbutton2;
    private bool _clickbutton2;

    private float AxisXmin_b1;
    private float AxisXmax_b1;
    private float AxisYmin_b1;
    private float AxisYmax_b1;

    private float AxisXmin_b2;
    private float AxisXmax_b2;
    private float AxisYmin_b2;
    private float AxisYmax_b2;

    private float AxisXmin_b3;
    private float AxisXmax_b3;
    private float AxisYmin_b3;
    private float AxisYmax_b3;
	// Use this for initialization
	void Start () {

        _onbutton = false;
        _clickbutton = false;
        _onbutton1 = false;
        _clickbutton1 = false;
	}
	
	// Update is called once per frame
	void Update () {
        AxisXmin_b1 = GameObject.Find("Main Camera").camera.pixelWidth * 0.137f;
        AxisXmax_b1 = GameObject.Find("Main Camera").camera.pixelWidth * 0.335f;
        AxisYmin_b1 = GameObject.Find("Main Camera").camera.pixelHeight * 0.752f;
        AxisYmax_b1 = GameObject.Find("Main Camera").camera.pixelHeight * 0.854f;

        AxisXmin_b2 = GameObject.Find("Main Camera").camera.pixelWidth * 0.392f;
        AxisXmax_b2 = GameObject.Find("Main Camera").camera.pixelWidth * 0.617f;
        AxisYmin_b2 = GameObject.Find("Main Camera").camera.pixelHeight * 0.752f;
        AxisYmax_b2 = GameObject.Find("Main Camera").camera.pixelHeight * 0.854f;

        AxisXmin_b3 = GameObject.Find("Main Camera").camera.pixelWidth * 0.664f;
        AxisXmax_b3 = GameObject.Find("Main Camera").camera.pixelWidth * 0.868f;
        AxisYmin_b3 = GameObject.Find("Main Camera").camera.pixelHeight * 0.752f;
        AxisYmax_b3 = GameObject.Find("Main Camera").camera.pixelHeight * 0.854f;
	}

    void OnGUI()
    {
        Event e = Event.current;
        //print(e.mousePosition);
        //print(((int)GameObject.Find("Main Camera").camera.pixelWidth) + " " + ((int)GameObject.Find("Main Camera").camera.pixelHeight));
        
        if ((e.mousePosition.x > AxisXmin_b1 && e.mousePosition.x < AxisXmax_b1) && (e.mousePosition.y > AxisYmin_b1 && e.mousePosition.y < AxisYmax_b1))
        {
            // 첫번째 (다시찍기 버튼 영역에 마우스가 들어오면)
            GameObject.Find("Button1").renderer.material.mainTexture = _texture[1];
            if (_onbutton2 == false)
            {
                _onbutton2 = true;
                GameObject.Find("AudioSource_ButtonOn").GetComponent<AudioSource>().Play();
            }

            if (Input.GetMouseButtonDown(0))    // 마우스 왼쪽 버튼이 클릭되면
            {
                if (_clickbutton2 == false)
                {
                    _clickbutton2 = true;
                    GameObject.Find("AudioSource_ButtonClick").GetComponent<AudioSource>().Play();
                }
                CaptureImage.time_start = true;              // 사진 찍기
                CaptureImage.isClick = true;
            }
            _clickbutton2 = false;
        }
        else
        {
            _onbutton2 = false;
            // 첫번째 (다시찍기 버튼 영역바깥에 마우스가 있으면)
            GameObject.Find("Button1").renderer.material.mainTexture = _texture[0];
        }

        if ((e.mousePosition.x > AxisXmin_b2 && e.mousePosition.x < AxisXmax_b2) && (e.mousePosition.y > AxisYmin_b2 && e.mousePosition.y < AxisYmax_b2))
        {
            // 첫번째 (다시찍기 버튼 영역에 마우스가 들어오면)
            GameObject.Find("Button2").renderer.material.mainTexture = _texture[3];
            if (_onbutton == false)
            {
                _onbutton = true;
                GameObject.Find("AudioSource_ButtonOn").GetComponent<AudioSource>().Play();
            }

            if (Input.GetMouseButtonDown(0))    // 마우스 왼쪽 버튼이 클릭되면
            {
                if (_clickbutton == false)
                {
                    _clickbutton = true;
                    GameObject.Find("AudioSource_ButtonClick").GetComponent<AudioSource>().Play();
                }
                CaptureImage.isClick = false;
                CaptureImage.isCapture = true;  // 다시찍기
            }
            _clickbutton = false;
        }
        else
        {
            _onbutton = false;
            // 첫번째 (다시찍기 버튼 영역바깥에 마우스가 있으면)
            GameObject.Find("Button2").renderer.material.mainTexture = _texture[2];
        }

        if ((e.mousePosition.x > AxisXmin_b3 && e.mousePosition.x < AxisXmax_b3) && (e.mousePosition.y > AxisYmin_b3 && e.mousePosition.y < AxisYmax_b3))
        {
            // 두번째 (다음단계로 버튼 영역에 마우스가 들어오면)
            GameObject.Find("Button3").renderer.material.mainTexture = _texture[5];
            if (_onbutton1 == false)
            {
                _onbutton1 = true;
                GameObject.Find("AudioSource_ButtonOn").GetComponent<AudioSource>().Play();
            }

            if (Input.GetMouseButtonDown(0))    // 마우스 왼쪽 버튼이 클릭되면
            {
                if (_clickbutton1 == false)     // 여기에 캡쳐가 안되면 안넘어가게끔 처리를 해 줘야함
                {
                    _clickbutton1 = true;
                    GameObject.Find("AudioSource_ButtonClick").GetComponent<AudioSource>().Play();
                    StartCoroutine("Delay");
                }
            }
            _clickbutton1 = false;
        }
        else
        {
            _onbutton1 = false;
            // 두번째 (다음단계로 버튼 영역바깥에 마우스가 있으면)
            GameObject.Find("Button3").renderer.material.mainTexture = _texture[4];
        }
        
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(0.5f);
        CaptureImage.OnDestroy();
        Application.LoadLevel("3.SelectMode");
        yield return null;
    }
}
