using UnityEngine;
using System.Collections;

public class FinalEvent : MonoBehaviour {

    public Texture[] _texture;

    private bool _onbutton;
    private bool _clickbutton;

    private float AxisXmin;
    private float AxisXmax;
    private float AxisYmin;
    private float AxisYmax;
	// Use this for initialization
	void Start () {

        _onbutton = false;
        _clickbutton = false;
	}
	
	// Update is called once per frame
	void Update () {

        AxisXmin = GameObject.Find("Main Camera").camera.pixelWidth * 0.357f;
        AxisXmax = GameObject.Find("Main Camera").camera.pixelWidth * 0.643f;
        AxisYmin = GameObject.Find("Main Camera").camera.pixelHeight * 0.829f;
        AxisYmax = GameObject.Find("Main Camera").camera.pixelHeight * 0.948f;
	}

    void OnGUI()
    {
        Event e = Event.current;
        if ((e.mousePosition.x > AxisXmin && e.mousePosition.x < AxisXmax) && (e.mousePosition.y > AxisYmin && e.mousePosition.y < AxisYmax))
        {
            // 첫번째 (초기화면으로 버튼 영역에 마우스가 들어오면)
            GameObject.Find("Button1").renderer.material.mainTexture = _texture[1];
            if (_onbutton == false)
            {
                _onbutton = true;
                GameObject.Find("AudioSource_ButtonOn").GetComponent<AudioSource>().Play();
            }
            if (Input.GetMouseButtonDown(0))    // 마우스 왼쪽 버튼이 클릭되면
            {
                if (_clickbutton == false)
                {
                    _clickbutton = true;
                    GameObject.Find("AudioSource_ButtonClick").GetComponent<AudioSource>().Play();
                    StartCoroutine("Delay");
                }
            }
            _clickbutton = false;
        }
        else
        {
            _onbutton = false;
            // 첫번째 (초기화면으로 버튼 영역바깥에 마우스가 있으면)
            GameObject.Find("Button1").renderer.material.mainTexture = _texture[0];
        }
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(0.4f);
        Application.LoadLevel("1.Openning");
        yield return null;
    }
}
