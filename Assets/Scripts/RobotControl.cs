using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ROBOTIS;
using System;
using System.Runtime.InteropServices;

public class RobotControl : MonoBehaviour
{
    [DllImport("dynamixel")]
    public static extern int dxl_initialize(int devIndex, int baudnum);

    // Control table address
    public const int P_GOAL_POSITION_L = 30;
    public const int P_GOAL_POSITION_H = 31;
    public const int P_GOAL_SPEED_L = 32;
    public const int P_GOAL_SPEED_H = 33;
    public const int P_TORQUE_LIMIT_L = 34;
    public const int P_TORQUE_LIMIT_H = 35;
    public const int P_CURRENT_POSITION_L = 36;
    public const int P_CURRENT_POSITION_H = 37;

    // Default setting
    public const int DEFAULT_BAUDNUM = 1; // 1Mbps
    public const int NUM_ACTUATOR = 8; // Number of actuator
    public const double STEP_THETA = Math.PI / 100; // Large value is more fast
    public const int CONTROL_PERIOD = 10; // msec (Large value is more slow) 

    public int GoalSpeed = 0;
    private string[] _portNames;
    public int portNumber = 7;
    private int[] GoalPos = new int[NUM_ACTUATOR];
    private int CommStatus;

    // Inverse kinematics
    private const float l1 = 10.8f;
    private const float l2 = 13.9f;
    private const float location_move_unit = 2f;
    private const int boundary_width = 20;
    private const int boundary_Height = 15;
    private const float move_unit = 0.1f;
    private const int move_pixel_unit = 5;

    private List<Point[]> segment_List_Right;
    private List<Point[]> segment_List_Left;
    private List<Point> Point_List_Right;
    private List<Point> Point_List_Left;

    public float xRight = l1 + l2;
    public float yRight = 0;
    public float xLeft = l1 + l2;
    public float yLeft = 0;
    public float LocationY_Left;
    public float LocationX_Left;
    public float LocationY_Right;
    public float LocationX_Right;

    public GameObject ProgramManager;

    public float progress;
    public int height;
    public int width;

    public bool on_off;
    public bool is_dynamixel_alive;

    void Awake()
    {
        _portNames = System.IO.Ports.SerialPort.GetPortNames();
        int portCount = _portNames.Length;
        
        for (int idx = 0; idx < portCount; idx++)
        {
            string numberString = _portNames[idx].Remove(0, 3);
            int number;
            if (int.TryParse(numberString, out number))
            {
                portNumber = number;
                print("port : " + number);
            }
            else
                print("Parsing failed : " + number);
        }
    }

    void Start()
    {
        ProgramManager = GameObject.Find("ProgramManager");

        width = 640;
        height = 480;

        Point_List_Right = new List<Point>();
        Point_List_Left = new List<Point>();
        segment_List_Right = new List<Point[]>();
        segment_List_Left = new List<Point[]>();

        progress = 0f;

        initialize_motors();
    }

    void OnDisable()
    {
        dynamixel.dxl_terminate();
    }

    public IEnumerator drawing_process_Array(List<Point[]> Line_list)
    {
        List<Point[]> segment_List = Line_list;

        float Current_point_X;
        float Current_point_Y;

        int current_pos_1;                      // 3번 모터 현재 위치 
        int current_pos_2;                      // 4번 모터 현재 위치 
        int dis_x;                              // 라인 X축 길이
        int dis_y;                              // 라인 Y축 길이
        int distance;                           // 라인 길이
        int angular_distance1;                  // 3번 모터의 이동할 전체 각도
        int angular_distance2;                  // 4번 모터의 이동할 전체 각도
        int angular_distance1_left;             // 3번 모터의 이동할 남은 각도
        int angular_distance2_left;             // 4번 모터의 이동할 남은 각도
        int most_left;                          // 3번 4번 모터 중 이동할 것이 많은 각도
        float k;                                // 시작점과 끝 점 사이 중간점의 위치 비율
        float d_k;                              // k의 증가값
        int velocity;                           // 모터 각속도

        double[] thetas;                        // InverseKinematics로 구한 각도값

        int i = 0;

        while (i < segment_List.Count)
        {
            if (is_dynamixel_alive == true)
            {
                ////////////////////////////////////////
                //시작으로 이동하기 위해서 계산할 것들//
                ////////////////////////////////////////

                Current_point_X = segment_List[i][0].getX();                                                 // 목표 픽셀 X좌표 (라인의 시작) 0~639
                Current_point_Y = segment_List[i][0].getY();                                                 // 목표 픽셀 Y좌표 (라인의 시작) 0~479

                if (Current_point_X >= width / 2)
                {
                    dynamixel.dxl_write_word(2, P_GOAL_POSITION_L, 412);
                    dynamixel.dxl_write_word(3, P_GOAL_POSITION_L, 512);
                    dynamixel.dxl_write_word(4, P_GOAL_POSITION_L, 512);

                    if(dynamixel.dxl_read_word(2, P_CURRENT_POSITION_L) != 412
                        || dynamixel.dxl_read_word(3, P_CURRENT_POSITION_L) != 512
                        || dynamixel.dxl_read_word(4, P_CURRENT_POSITION_L) != 512
                        )
                    {
                        yield return new WaitForSeconds(1f);
                    }

                    current_pos_1 = dynamixel.dxl_read_word(7, P_CURRENT_POSITION_L);         // 7번 모터의 현재 각도 설정 값 0~1023
                    current_pos_2 = dynamixel.dxl_read_word(8, P_CURRENT_POSITION_L);         // 8번 모터의 현재 각도 설정 값 0~1023

                    //LocationX_Left = 13f - (float)(yLeft) * ((float)boundary_Height) / (float)height;
                    LocationX_Left = 14.4f - (float)(Current_point_Y) * ((float)boundary_Height) / (float)height * 1.03f;
                    LocationY_Left = 19.3f - (float)(Current_point_X - width * 0.5f) * ((float)boundary_width) / (float)width;

                    //print("L + M : " + LocationX_Left + " " + LocationY_Left);

                    thetas = inverse_Kinematic(LocationX_Left, LocationY_Left);                         // 모터들 값 Inverse kinematics 한 각도 값 저장

                    GoalPos[6] = 512 + (int)(3.413 * 180 / Math.PI * thetas[0]);                        // 각도 값을 모터 값으로 변환 저장
                    GoalPos[7] = 512 - (int)(3.413 * 180 / Math.PI * thetas[1]);                        // 각도 값을 모터 값으로 변환 저장

                    angular_distance1 = current_pos_1 - GoalPos[6];                                     // 목표 각도까지 각도 값 저장
                    angular_distance2 = current_pos_2 - GoalPos[7];                                     // 목표 각도까지 각도 값 저장

                    dynamixel.dxl_write_word(7, P_GOAL_POSITION_L, GoalPos[6]);                         // 7번 모터의 현재 각도값 전송 0~1023
                    dynamixel.dxl_write_word(8, P_GOAL_POSITION_L, GoalPos[7]);                         // 8번 모터의 현재 각도값 전송 0~1023

                    do
                    {                                                                                   // 도착할 때까지 모터 속도 값 패킷 전송
                        current_pos_1 = dynamixel.dxl_read_word(7, P_CURRENT_POSITION_L);               // 7번 모터의 현재 각도 설정 값 0~1023
                        current_pos_2 = dynamixel.dxl_read_word(8, P_CURRENT_POSITION_L);               // 8번 모터의 현재 각도 설정 값 0~1023

                        //print("current degree 1, 2 : " + current_pos_1 + ", " + current_pos_2);
                        //print("goal    degree 1, 2 : " + GoalPos[6] + ", " + GoalPos[7]);

                        angular_distance1_left = Math.Abs(current_pos_1 - GoalPos[6]);                  // 목표 각도까지 남은 각도 값 저장
                        angular_distance2_left = Math.Abs(current_pos_2 - GoalPos[7]);                  // 목표 각도까지 남은 각도 값 저장

                        //print("left    degree 1, 2 : " + angular_distance1_left + ", " + angular_distance2_left);

                        if (angular_distance1_left > angular_distance2_left)
                        {
                            // cos 방법
                            velocity = (int)(((1 - Math.Cos((Math.PI * 2.0) *
                                (float)angular_distance1_left / (float)angular_distance1))) * 50) + 50;
                            //print("v1  : " + velocity);
                            dynamixel.dxl_write_word(7, P_GOAL_SPEED_L, velocity);
                            dynamixel.dxl_write_word(8, P_GOAL_SPEED_L, velocity);
                            most_left = angular_distance1_left;
                        }
                        else
                        {
                            // cos 방법
                            velocity = (int)(((1 - Math.Cos((Math.PI * 2.0) *
                                (float)angular_distance2_left / (float)angular_distance2))) * 50) + 50;
                            //print("v2  : " + velocity);
                            dynamixel.dxl_write_word(7, P_GOAL_SPEED_L, velocity);
                            dynamixel.dxl_write_word(8, P_GOAL_SPEED_L, velocity);
                            most_left = angular_distance2_left;
                        }
                        //print("m : " + most_left);
                        yield return new WaitForSeconds(0.1f);

                    } while (most_left > 3);

                    GoalPos[5] = 512 + 192;
                    dynamixel.dxl_write_word(6, P_GOAL_POSITION_L, GoalPos[5]);

                    yield return new WaitForSeconds(0.3f);

                    // 여기 라인 시작 전 도착
                    // 라인 그리기

                    dis_x = segment_List[i][1].getX() - segment_List[i][0].getX();              // 라인의 X축 거리
                    dis_y = segment_List[i][1].getY() - segment_List[i][0].getY();              // 라인의 Y축 거리
                    distance = (int)Math.Sqrt((dis_x * dis_x) + (dis_y * dis_y));               // 두 점 사이의 거리 계산 (피타고라스)

                    //print(segment_List[i][0].getX() + ", " + segment_List[i][0].getY());

                    d_k = (float)move_pixel_unit / (float)distance;                             // 시작점과 끝점 거리에 따른 k증가값 계산
                    //print(d_k);

                    //print("m b : " + dynamixel.dxl_read_word(7, P_CURRENT_POSITION_L) + ", " + dynamixel.dxl_read_word(8, P_CURRENT_POSITION_L));

                    for (k = 0f; k < 1f + d_k; k += d_k)                                             // 라인따라 k_d씩 이동
                    {
                        Current_point_X = segment_List[i][0].getX()
                            + k * (segment_List[i][1].getX() - segment_List[i][0].getX());      // 중간점의 x축 픽셀 좌표 계산 0~639
                        Current_point_Y = segment_List[i][0].getY()
                            + k * (segment_List[i][1].getY() - segment_List[i][0].getY());      // 중간점의 y축 픽셀 좌표 계산 0~479

                        //print("L + D : " + Current_point_X + " , " + Current_point_Y);

                        LocationX_Left = 14.4f - (float)(Current_point_Y) * ((float)boundary_Height) / (float)height * 1.03f;               // 2픽셀 이동 후 종이 상의 X좌표 
                        LocationY_Left = 19.3f - (float)(Current_point_X - width * 0.5f) * ((float)boundary_width) / (float)width;  // 2픽셀 이동 후 종이 상의 X좌표

                        //print("L + D : " + LocationX_Left + " " + LocationY_Left);

                        thetas = inverse_Kinematic(LocationX_Left, LocationY_Left);
                        if (thetas != null)
                        {
                            GoalPos[6] = 512 + (int)(3.413 * 180 / Math.PI * thetas[0]);
                            GoalPos[7] = 512 - (int)(3.413 * 180 / Math.PI * thetas[1]);

                            dynamixel.dxl_write_word(7, P_GOAL_POSITION_L, GoalPos[6]);
                            dynamixel.dxl_write_word(8, P_GOAL_POSITION_L, GoalPos[7]);

                            //CommStatus = send_packet_with_array(P_GOAL_POSITION_L, GoalPos);
                            //get_result(CommStatus);

                            yield return new WaitForSeconds(0.1f);
                        }
                        else
                        {
                            segment_List.Add(
                                new Point[] { new Point((int)Current_point_X, (int)Current_point_Y), new Point(segment_List[i][1].getX(), segment_List[i][1].getY()) });
                            break;
                        }
                    }

                    //print("m a : " + dynamixel.dxl_read_word(7, P_CURRENT_POSITION_L) + ", " + dynamixel.dxl_read_word(8, P_CURRENT_POSITION_L));

                    yield return new WaitForSeconds(0.1f);                                              // 기다림

                    GoalPos[5] = 612;                                                                   // 올리는 각도
                    dynamixel.dxl_write_word(6, P_GOAL_POSITION_L, GoalPos[5]);                         // 6번 모터에 올리는 각도 전송

                    yield return new WaitForSeconds(0.2f);                                              // 기다림

                    i++;
                }
                else
                {
                    dynamixel.dxl_write_word(6, P_GOAL_POSITION_L, 612);
                    dynamixel.dxl_write_word(7, P_GOAL_POSITION_L, 512);
                    dynamixel.dxl_write_word(8, P_GOAL_POSITION_L, 512);

                    if (dynamixel.dxl_read_word(6, P_CURRENT_POSITION_L) != 612
                        || dynamixel.dxl_read_word(7, P_CURRENT_POSITION_L) != 512
                        || dynamixel.dxl_read_word(8, P_CURRENT_POSITION_L) != 512
                        )
                    {
                        yield return new WaitForSeconds(1f);
                    }

                    current_pos_1 = dynamixel.dxl_read_word(3, P_CURRENT_POSITION_L);                   // 3번 모터의 현재 각도 설정 값 0~1023
                    current_pos_2 = dynamixel.dxl_read_word(4, P_CURRENT_POSITION_L);                   // 4번 모터의 현재 각도 설정 값 0~1023

                    LocationX_Right = 14.5f - (float)Current_point_Y * ((float)boundary_Height) / (float)height;   // 종이 상의 목표 X좌표 
                    LocationY_Right = 18.5f - ((float)boundary_width) * 0.5f
                                            + (float)Current_point_X * ((float)boundary_width) / (float)width;   // 종이 상의 목표 Y좌표 

                    thetas = inverse_Kinematic(LocationX_Right, LocationY_Right);                       // 각도 값을 Inverse kinematics 한 각도 값 저장
                    GoalPos[2] = 512 - (int)(3.413 * 180 / Math.PI * thetas[0]);                        // 각도 값을 모터 값으로 변환 저장
                    GoalPos[3] = 512 + (int)(3.413 * 180 / Math.PI * thetas[1]);                        // 각도 값을 모터 값으로 변환 저장

                    angular_distance1 = current_pos_1 - GoalPos[2];                                     // 목표 각도까지 각도 값 저장
                    angular_distance2 = current_pos_2 - GoalPos[3];                                     // 목표 각도까지 각도 값 저장

                    dynamixel.dxl_write_word(3, P_GOAL_POSITION_L, GoalPos[2]);                         // 3번 모터 목표 각도 값 전송
                    dynamixel.dxl_write_word(4, P_GOAL_POSITION_L, GoalPos[3]);                         // 4번 모터 목표 각도 값 전송

                    do
                    {                                                                                   // 도착할 때까지 모터 속도 값 패킷 전송
                        current_pos_1 = dynamixel.dxl_read_word(3, P_CURRENT_POSITION_L);               // 3번 모터의 현재 각도값 전송 0~1023
                        current_pos_2 = dynamixel.dxl_read_word(4, P_CURRENT_POSITION_L);               // 4번 모터의 현재 각도값 전송 0~1023

                        angular_distance1_left = Math.Abs(current_pos_1 - GoalPos[2]);                  // 목표 각도까지 남은 각도값 저장
                        angular_distance2_left = Math.Abs(current_pos_2 - GoalPos[3]);                  // 목표 각도까지 남은 각도값 저장

                        if (angular_distance1_left > angular_distance2_left)                            // 모터 2개중 이동해야할 각도 비교
                        {
                            // cos 방법
                            velocity = (int)(((1 - Math.Cos((Math.PI * 2.0) *
                                (float)angular_distance1_left / (float)angular_distance1))) * 50) + 50;// 거리에 따른 속도 계산 50~150
                            dynamixel.dxl_write_word(3, P_GOAL_SPEED_L, velocity);                      // 3번 모터 속도값 전송
                            dynamixel.dxl_write_word(4, P_GOAL_SPEED_L, velocity);                      // 4번 모터 속도값 전송
                            most_left = angular_distance1_left;                                         // 최대 각도 값 저장
                        }
                        else
                        {
                            // cos 방법
                            velocity = (int)(((1 - Math.Cos((Math.PI * 2.0) *
                                (float)angular_distance2_left / (float)angular_distance2))) * 50) + 50;// 거리에 따른 속도 계산 50~150
                            dynamixel.dxl_write_word(3, P_GOAL_SPEED_L, velocity);                      // 3번 모터 속도값 전송
                            dynamixel.dxl_write_word(4, P_GOAL_SPEED_L, velocity);                      // 4번 모터 속도값 전송
                            most_left = angular_distance2_left;                                         // 최대 각도 값 저장
                        }
                        yield return new WaitForSeconds(0.1f);                                          // 10Hz 속도값 전송
                    } while (most_left > 3);                                                            // 목적 각도와 현재 각도 값이 3이내 일 때 속도 값 전송 종료

                    /////////////////////////////////
                    //라인 시작점 도착 및 펜 내리기//
                    /////////////////////////////////
                    yield return new WaitForSeconds(0.1f);                                              // 기다림
                    GoalPos[1] = 512 - 202;                                                             // 내리는 각도
                    dynamixel.dxl_write_word(2, P_GOAL_POSITION_L, GoalPos[1]);                         // 2번 모터에 내리는 각도 값 전송
                    yield return new WaitForSeconds(0.3f);                                              // 기다림

                    ///////////////
                    //라인 그리기//
                    ///////////////
                    dis_x = segment_List[i][1].getX() - segment_List[i][0].getX();                      // 라인의 X축 거리
                    dis_y = segment_List[i][1].getY() - segment_List[i][0].getY();                      // 라인의 Y축 거리
                    distance = (int)Math.Sqrt((dis_x * dis_x) + (dis_y * dis_y));                       // 두 점 사이의 거리 계산 (피타고라스)

                    d_k = (float)move_pixel_unit / (float)distance;                                     // 시작점과 끝점 거리에 따른 k증가값 계산

                    for (k = 0f; k < 1f + d_k; k += d_k)                                                     // 라인따라 k_d씩 이동
                    {
                        Current_point_X = segment_List[i][0].getX()
                            + k * (segment_List[i][1].getX() - segment_List[i][0].getX());              // 중간점의 x축 픽셀 좌표 계산 0~639
                        Current_point_Y = segment_List[i][0].getY()
                            + k * (segment_List[i][1].getY() - segment_List[i][0].getY());              // 중간점의 y축 픽셀 좌표 계산 0~479

                        LocationX_Right = 14.5f - (float)Current_point_Y * ((float)boundary_Height) / (float)height;// 중간점의 종이 상의 X좌표 
                        LocationY_Right = 18.5f - ((float)boundary_width) * 0.5f
                                            + (float)Current_point_X * ((float)boundary_width) / (float)width;   // 중간점의 종이 상의 Y좌표

                        thetas = inverse_Kinematic(LocationX_Right, LocationY_Right);                   // 좌표들 값을 Inverse kinematics 한 각도 값 저장
                        if (thetas != null)
                        {
                            GoalPos[2] = 512 - (int)(3.413 * 180 / Math.PI * thetas[0]);                    // 각도 값을 모터 값으로 변환 저장
                            GoalPos[3] = 512 + (int)(3.413 * 180 / Math.PI * thetas[1]);                    // 각도 값을 모터 값으로 변환 저장

                            dynamixel.dxl_write_word(3, P_GOAL_POSITION_L, GoalPos[2]);                     // 3번 모터 속도값 전송
                            dynamixel.dxl_write_word(4, P_GOAL_POSITION_L, GoalPos[3]);                     // 4번 모터 속도값 전송

                            yield return new WaitForSeconds(0.1f);                                          // 5Hz 속도값 전송
                        }
                        else
                        {
                            segment_List.Add(
                                new Point[] { new Point((int)Current_point_X, (int)Current_point_Y), new Point(segment_List[i][1].getX(), segment_List[i][1].getY()) });
                            break;
                        }
                    }

                    yield return new WaitForSeconds(0.1f);                                              // 기다림

                    GoalPos[1] = 412;                                                                   // 올리는 각도
                    dynamixel.dxl_write_word(2, P_GOAL_POSITION_L, GoalPos[1]);                         // 2번 모터 각도값 전송

                    yield return new WaitForSeconds(0.2f);                                              // 기다림

                    i++;
                }

                progress += (float)(1f / (float)segment_List.Count * 100f);
            }
            else
            {
                yield return new WaitForSeconds(0.5f);
            }
        }

        dynamixel.dxl_write_word(2, P_GOAL_POSITION_L, 412);
        dynamixel.dxl_write_word(3, P_GOAL_POSITION_L, 512);
        dynamixel.dxl_write_word(4, P_GOAL_POSITION_L, 512);
        dynamixel.dxl_write_word(6, P_GOAL_POSITION_L, 612);
        dynamixel.dxl_write_word(7, P_GOAL_POSITION_L, 512);
        dynamixel.dxl_write_word(8, P_GOAL_POSITION_L, 512);

        yield return null;
    }

    public IEnumerator drawing_process_List_Right()
    {
        Point_List_Right = ImageProcessing.Trace_Point_List_Right;

        int dis_x;                              // 라인 X축 길이
        int dis_y;                              // 라인 Y축 길이
        int down_pos;
        int distance;                           // 라인 길이
        double[] thetas;                        // InverseKinematics로 구한 각도값

        for (int i = 0; i < Point_List_Right.Count; i++)
        {
            if (is_dynamixel_alive)
            {
                xRight = Point_List_Right[i].getX();              // 중간점의 x축 픽셀 좌표 계산 0~639
                yRight = Point_List_Right[i].getY();              // 중간점의 y축 픽셀 좌표 계산 0~479

                //print("R : " + xRight + ", " + yRight);

                LocationX_Right = 14.5f - (float)yRight * ((float)boundary_Height) / (float)height;     // 중간점의 종이 상의 X좌표 
                LocationY_Right = 18.5f - ((float)boundary_width) * 0.5f
                                    + (float)xRight * ((float)boundary_width) / (float)width;           // 중간점의 종이 상의 Y좌표

                //print(LocationX_Right + ", " + LocationY_Right);

                thetas = inverse_Kinematic(LocationX_Right, LocationY_Right);                           // 좌표들 값을 Inverse kinematics 한 각도 값 저장
                
                if (thetas != null)
                {
                    GoalPos[2] = 512 - (int)(Math.Round(3.413 * 180 / Math.PI * thetas[0]));                // 각도 값을 모터 값으로 변환 저장
                    GoalPos[3] = 512 + (int)(Math.Round(3.413 * 180 / Math.PI * thetas[1]));                // 각도 값을 모터 값으로 변환 저장

                    dynamixel.dxl_write_word(3, P_GOAL_POSITION_L, GoalPos[2]);                             // 3번 모터 속도값 전송
                    dynamixel.dxl_write_word(4, P_GOAL_POSITION_L, GoalPos[3]);                             // 4번 모터 속도값 전송

                    yield return new WaitForSeconds(0.1f);                                                  // 5Hz 속도값 전송

                    down_pos = dynamixel.dxl_read_word(2, P_GOAL_POSITION_L);

                    if ((Math.Abs(down_pos - (512 - 202))) > 3)
                    {
                        yield return new WaitForSeconds(0.2f + Math.Abs(down_pos - (512 - 202)) * 0.004f);  // 기다림
                        GoalPos[1] = 512 - 202;                                                             // 내리는 각도
                        dynamixel.dxl_write_word(2, P_GOAL_POSITION_L, GoalPos[1]);                         // 2번 모터에 내리는 각도 값 전송
                        yield return new WaitForSeconds(0.1f);
                    }

                    if (i != Point_List_Right.Count - 1)
                    {
                        dis_x = Point_List_Right[i].getX() - Point_List_Right[i + 1].getX();
                        dis_y = Point_List_Right[i].getY() - Point_List_Right[i + 1].getY();
                        distance = (int)((dis_x * dis_x) + (dis_y * dis_y));

                        if (distance > 18)
                        {
                            GoalPos[1] = 412;                                                               // 올리는 각도
                            dynamixel.dxl_write_word(2, P_GOAL_POSITION_L, GoalPos[1]);                     // 2번 모터 각도값 전송

                            yield return new WaitForSeconds(0.2f);
                        }
                    }
                    progress += (float)(0.5f / (float)Point_List_Right.Count * 100f);
                }
                else
                {
                    Point_List_Left.Add(Point_List_Right[i]);
                }
            }
            else
            {
                i--;
                yield return new WaitForSeconds(0.2f);
            }
        }

        dynamixel.dxl_write_word(2, P_GOAL_POSITION_L, 412);
        dynamixel.dxl_write_word(3, P_GOAL_POSITION_L, 512);
        dynamixel.dxl_write_word(4, P_GOAL_POSITION_L, 512);

        yield return null;
    }

    public IEnumerator drawing_process_List_Left()
    {
        Point_List_Left = ImageProcessing.Trace_Point_List_Left;

        yield return new WaitForSeconds(1f);

        int dis_x;                              // 라인 X축 길이
        int dis_y;                              // 라인 Y축 길이
        int down_pos;
        int distance;                           // 라인 길이
        double[] thetas;                        // InverseKinematics로 구한 각도값

        for (int i = 0; i < Point_List_Left.Count; i++)
        {
            if (is_dynamixel_alive)
            {
                xLeft = Point_List_Left[i].getX();              // 중간점의 x축 픽셀 좌표 계산 0~639
                yLeft = Point_List_Left[i].getY();              // 중간점의 y축 픽셀 좌표 계산 0~479

                //print("L : " + xLeft + ", " + yLeft);

                LocationX_Left = 14.4f - (float)yLeft * ((float)boundary_Height) / (float)height * 1.03f;// 중간점의 종이 상의 X좌표 
                LocationY_Left = 19.3f - (float)(xLeft - width * 0.5f) * ((float)boundary_width) / (float)width;

                thetas = inverse_Kinematic(LocationX_Left, LocationY_Left);                   // 좌표들 값을 Inverse kinematics 한 각도 값 저장
                if (thetas != null)
                {
                    GoalPos[6] = 512 + (int)(Math.Round(3.413 * 180 / Math.PI * thetas[0]));                // 각도 값을 모터 값으로 변환 저장
                    GoalPos[7] = 512 - (int)(Math.Round(3.413 * 180 / Math.PI * thetas[1]));                   // 각도 값을 모터 값으로 변환 저장

                    dynamixel.dxl_write_word(7, P_GOAL_POSITION_L, GoalPos[6]);                     // 3번 모터 속도값 전송
                    dynamixel.dxl_write_word(8, P_GOAL_POSITION_L, GoalPos[7]);                     // 4번 모터 속도값 전송

                    yield return new WaitForSeconds(0.1f);

                    down_pos = dynamixel.dxl_read_word(6, P_GOAL_POSITION_L);
                
                    if (Math.Abs((down_pos - (512 + 196))) > 3)
                    {
                        yield return new WaitForSeconds(0.2f + Math.Abs(down_pos - (512 + 196)) * 0.004f);                                              // 기다림
                        GoalPos[5] = 512 + 196;                                                             // 내리는 각도
                        dynamixel.dxl_write_word(6, P_GOAL_POSITION_L, GoalPos[5]);                         // 2번 모터에 내리는 각도 값 전송
                        yield return new WaitForSeconds(0.1f);
                    }

                    if (i != Point_List_Left.Count - 1)
                    {
                        dis_x = Point_List_Left[i].getX() - Point_List_Left[i + 1].getX();
                        dis_y = Point_List_Left[i].getY() - Point_List_Left[i + 1].getY();
                        distance = (int)((dis_x * dis_x) + (dis_y * dis_y));

                        if (distance > 18)
                        {
                            GoalPos[5] = 612;                                                                   // 올리는 각도
                            dynamixel.dxl_write_word(6, P_GOAL_POSITION_L, GoalPos[5]);                         // 2번 모터 각도값 전송

                            yield return new WaitForSeconds(0.2f);
                        }
                    }
                    progress = ((float)i / (float)(Point_List_Left.Count - 1f)) * 100f;
                }
                else
                {
                    Point_List_Right.Add(Point_List_Left[i]);
                }
            }
            else
            {
                i--;
                yield return new WaitForSeconds(0.2f);
            }
        }

        dynamixel.dxl_write_word(6, P_GOAL_POSITION_L, 612);
        dynamixel.dxl_write_word(7, P_GOAL_POSITION_L, 512);
        dynamixel.dxl_write_word(8, P_GOAL_POSITION_L, 512);

        yield return null;
    }

    public IEnumerator drawing_process_Array_Right(List<Point[]> Line_list)
    {
        segment_List_Right = Line_list;

        int current_pos_1;                      // 3번 모터 현재 위치 
        int current_pos_2;                      // 4번 모터 현재 위치 
        int dis_x;                              // 라인 X축 길이
        int dis_y;                              // 라인 Y축 길이
        int distance;                           // 라인 길이
        int angular_distance1;                  // 3번 모터의 이동할 전체 각도
        int angular_distance2;                  // 4번 모터의 이동할 전체 각도
        int angular_distance1_left;             // 3번 모터의 이동할 남은 각도
        int angular_distance2_left;             // 4번 모터의 이동할 남은 각도
        int most_left;                          // 3번 4번 모터 중 이동할 것이 많은 각도
        float k;                                // 시작점과 끝 점 사이 중간점의 위치 비율
        float d_k;                              // k의 증가값
        int velocity;                           // 모터 각속도

        double[] thetas;                        // InverseKinematics로 구한 각도값

        int i = 0;

        while (i < segment_List_Right.Count)
        {
            if (is_dynamixel_alive == true)
            {
                ////////////////////////////////////////
                //시작으로 이동하기 위해서 계산할 것들//
                ////////////////////////////////////////
                current_pos_1 = dynamixel.dxl_read_word(3, P_CURRENT_POSITION_L);                   // 3번 모터의 현재 각도 설정 값 0~1023
                current_pos_2 = dynamixel.dxl_read_word(4, P_CURRENT_POSITION_L);                   // 4번 모터의 현재 각도 설정 값 0~1023

                xRight = segment_List_Right[i][0].getX();                                                 // 목표 픽셀 X좌표 (라인의 시작) 0~639
                yRight = segment_List_Right[i][0].getY();                                                 // 목표 픽셀 Y좌표 (라인의 시작) 0~479

                LocationX_Right = 14.5f - (float)yRight * ((float)boundary_Height) / (float)height;   // 종이 상의 목표 X좌표 
                LocationY_Right = 18.5f - ((float)boundary_width) * 0.5f
                                        + (float)xRight * ((float)boundary_width) / (float)width;   // 종이 상의 목표 Y좌표 

                thetas = inverse_Kinematic(LocationX_Right, LocationY_Right);                       // 각도 값을 Inverse kinematics 한 각도 값 저장
                GoalPos[2] = 512 - (int)(3.413 * 180 / Math.PI * thetas[0]);                        // 각도 값을 모터 값으로 변환 저장
                GoalPos[3] = 512 + (int)(3.413 * 180 / Math.PI * thetas[1]);                        // 각도 값을 모터 값으로 변환 저장

                angular_distance1 = current_pos_1 - GoalPos[2];                                     // 목표 각도까지 각도 값 저장
                angular_distance2 = current_pos_2 - GoalPos[3];                                     // 목표 각도까지 각도 값 저장

                dynamixel.dxl_write_word(3, P_GOAL_POSITION_L, GoalPos[2]);                         // 3번 모터 목표 각도 값 전송
                dynamixel.dxl_write_word(4, P_GOAL_POSITION_L, GoalPos[3]);                         // 4번 모터 목표 각도 값 전송

                do
                {                                                                                   // 도착할 때까지 모터 속도 값 패킷 전송
                    current_pos_1 = dynamixel.dxl_read_word(3, P_CURRENT_POSITION_L);               // 3번 모터의 현재 각도값 전송 0~1023
                    current_pos_2 = dynamixel.dxl_read_word(4, P_CURRENT_POSITION_L);               // 4번 모터의 현재 각도값 전송 0~1023

                    angular_distance1_left = Math.Abs(current_pos_1 - GoalPos[2]);                  // 목표 각도까지 남은 각도값 저장
                    angular_distance2_left = Math.Abs(current_pos_2 - GoalPos[3]);                  // 목표 각도까지 남은 각도값 저장

                    if (angular_distance1_left > angular_distance2_left)                            // 모터 2개중 이동해야할 각도 비교
                    {
                        // cos 방법
                        velocity = (int)(((1 - Math.Cos((Math.PI * 2.0) *
                            (float)angular_distance1_left / (float)angular_distance1))) * 50) + 50;// 거리에 따른 속도 계산 50~150
                        dynamixel.dxl_write_word(3, P_GOAL_SPEED_L, velocity);                      // 3번 모터 속도값 전송
                        dynamixel.dxl_write_word(4, P_GOAL_SPEED_L, velocity);                      // 4번 모터 속도값 전송
                        most_left = angular_distance1_left;                                         // 최대 각도 값 저장
                    }
                    else
                    {
                        // cos 방법
                        velocity = (int)(((1 - Math.Cos((Math.PI * 2.0) *
                            (float)angular_distance2_left / (float)angular_distance2))) * 50) + 50;// 거리에 따른 속도 계산 50~150
                        dynamixel.dxl_write_word(3, P_GOAL_SPEED_L, velocity);                      // 3번 모터 속도값 전송
                        dynamixel.dxl_write_word(4, P_GOAL_SPEED_L, velocity);                      // 4번 모터 속도값 전송
                        most_left = angular_distance2_left;                                         // 최대 각도 값 저장
                    }
                    yield return new WaitForSeconds(0.1f);                                          // 10Hz 속도값 전송
                } while (most_left > 3);                                                            // 목적 각도와 현재 각도 값이 3이내 일 때 속도 값 전송 종료

                /////////////////////////////////
                //라인 시작점 도착 및 펜 내리기//
                /////////////////////////////////
                yield return new WaitForSeconds(0.1f);                                              // 기다림
                GoalPos[1] = 512 - 202;                                                             // 내리는 각도
                dynamixel.dxl_write_word(2, P_GOAL_POSITION_L, GoalPos[1]);                         // 2번 모터에 내리는 각도 값 전송
                yield return new WaitForSeconds(0.3f);                                              // 기다림

                ///////////////
                //라인 그리기//
                ///////////////
                dis_x = segment_List_Right[i][1].getX() - segment_List_Right[i][0].getX();                      // 라인의 X축 거리
                dis_y = segment_List_Right[i][1].getY() - segment_List_Right[i][0].getY();                      // 라인의 Y축 거리
                distance = (int)Math.Sqrt((dis_x * dis_x) + (dis_y * dis_y));                       // 두 점 사이의 거리 계산 (피타고라스)

                d_k = (float)move_pixel_unit / (float)distance;                                     // 시작점과 끝점 거리에 따른 k증가값 계산

                for (k = 0f; k < 1f + d_k; k += d_k)                                                     // 라인따라 k_d씩 이동
                {
                    xRight = segment_List_Right[i][0].getX()
                        + k * (segment_List_Right[i][1].getX() - segment_List_Right[i][0].getX());              // 중간점의 x축 픽셀 좌표 계산 0~639
                    yRight = segment_List_Right[i][0].getY()
                        + k * (segment_List_Right[i][1].getY() - segment_List_Right[i][0].getY());              // 중간점의 y축 픽셀 좌표 계산 0~479

                    LocationX_Right = 14.5f - (float)yRight * ((float)boundary_Height) / (float)height;// 중간점의 종이 상의 X좌표 
                    LocationY_Right = 18.5f - ((float)boundary_width) * 0.5f
                                        + (float)xRight * ((float)boundary_width) / (float)width;   // 중간점의 종이 상의 Y좌표

                    thetas = inverse_Kinematic(LocationX_Right, LocationY_Right);                   // 좌표들 값을 Inverse kinematics 한 각도 값 저장
                    if (thetas != null)
                    {
                        GoalPos[2] = 512 - (int)(3.413 * 180 / Math.PI * thetas[0]);                    // 각도 값을 모터 값으로 변환 저장
                        GoalPos[3] = 512 + (int)(3.413 * 180 / Math.PI * thetas[1]);                    // 각도 값을 모터 값으로 변환 저장

                        dynamixel.dxl_write_word(3, P_GOAL_POSITION_L, GoalPos[2]);                     // 3번 모터 속도값 전송
                        dynamixel.dxl_write_word(4, P_GOAL_POSITION_L, GoalPos[3]);                     // 4번 모터 속도값 전송

                        yield return new WaitForSeconds(0.2f);                                          // 5Hz 속도값 전송
                    }
                    else
                    {
                        segment_List_Left.Add(
                            new Point[] { new Point((int)xRight, (int)yRight), new Point(segment_List_Right[i][1].getX(), segment_List_Right[i][1].getY()) });
                        break;
                    }
                }

                yield return new WaitForSeconds(0.1f);                                              // 기다림

                GoalPos[1] = 412;                                                                   // 올리는 각도
                dynamixel.dxl_write_word(2, P_GOAL_POSITION_L, GoalPos[1]);                         // 2번 모터 각도값 전송

                yield return new WaitForSeconds(0.3f);                                              // 기다림

                progress += (float)(1f / (float)segment_List_Right.Count * 100f);
                i++; 
                                                                               // 다음 라인으로
            }
            else
            {
                yield return new WaitForSeconds(0.5f);
            }
        }

        dynamixel.dxl_write_word(2, P_GOAL_POSITION_L, 412);
        dynamixel.dxl_write_word(3, P_GOAL_POSITION_L, 512);
        dynamixel.dxl_write_word(4, P_GOAL_POSITION_L, 512);

        yield return null;
    }

    public IEnumerator drawing_process_Array_Left(List<Point[]> Line_list)
    {
        //List<Point[]> segment_List = ImageProcessing.Hough_Line_List_Left;
        segment_List_Left = Line_list;

        yield return new WaitForSeconds(1f);

        int current_pos_1;                      // 7번 모터 현재 위치 
        int current_pos_2;                      // 번 모터 현재 위치 
        int dis_x;                              // 라인 X축 길이
        int dis_y;                              // 라인 Y축 길이
        int distance;                           // 라인 길이
        int angular_distance1;                  // 7번 모터의 이동할 전체 각도
        int angular_distance2;                  // 8번 모터의 이동할 전체 각도
        int angular_distance1_left;             // 7번 모터의 이동할 남은 각도
        int angular_distance2_left;             // 8번 모터의 이동할 남은 각도
        int most_left;                          // 4번 8번 모터 중 이동할 것이 많은 각도
        float k;                                // 시작점과 끝 점 사이 중간점의 위치 비율
        float d_k;                              // k의 증가값
        int velocity;                           // 모터 각속도

        double[] thetas;                        // InverseKinematics로 구한 각도값

        int i = 0;

        while (i < segment_List_Left.Count)
        {
            if (is_dynamixel_alive == true)
            {
                // 시작으로 이동하기 위해서 계산할 것들

                current_pos_1 = dynamixel.dxl_read_word(7, P_CURRENT_POSITION_L);         // 7번 모터의 현재 각도 설정 값 0~1023
                current_pos_2 = dynamixel.dxl_read_word(8, P_CURRENT_POSITION_L);         // 8번 모터의 현재 각도 설정 값 0~1023

                xLeft = segment_List_Left[i][0].getX();      // 목표 픽셀 X좌표 (라인의 시작) 0~639
                yLeft = segment_List_Left[i][0].getY();      // 목표 픽셀 Y좌표 (라인의 시작) 0~479

                //print("L + M : " + xLeft + " , " + yLeft);

                //LocationX_Left = 13f - (float)(yLeft) * ((float)boundary_Height) / (float)height;
                LocationX_Left = 14.4f - (float)(yLeft) * ((float)boundary_Height) / (float)height * 1.03f;
                LocationY_Left = 19.3f - (float)(xLeft - width * 0.5f) * ((float)boundary_width) / (float)width;

                //print("L + M : " + LocationX_Left + " " + LocationY_Left);

                thetas = inverse_Kinematic(LocationX_Left, LocationY_Left);                         // 모터들 값 Inverse kinematics 한 각도 값 저장

                GoalPos[6] = 512 + (int)(3.413 * 180 / Math.PI * thetas[0]);                        // 각도 값을 모터 값으로 변환 저장
                GoalPos[7] = 512 - (int)(3.413 * 180 / Math.PI * thetas[1]);                        // 각도 값을 모터 값으로 변환 저장

                angular_distance1 = current_pos_1 - GoalPos[6];                                     // 목표 각도까지 각도 값 저장
                angular_distance2 = current_pos_2 - GoalPos[7];                                     // 목표 각도까지 각도 값 저장

                dynamixel.dxl_write_word(7, P_GOAL_POSITION_L, GoalPos[6]);                         // 7번 모터의 현재 각도값 전송 0~1023
                dynamixel.dxl_write_word(8, P_GOAL_POSITION_L, GoalPos[7]);                         // 8번 모터의 현재 각도값 전송 0~1023

                do
                {                                                                                   // 도착할 때까지 모터 속도 값 패킷 전송
                    current_pos_1 = dynamixel.dxl_read_word(7, P_CURRENT_POSITION_L);               // 7번 모터의 현재 각도 설정 값 0~1023
                    current_pos_2 = dynamixel.dxl_read_word(8, P_CURRENT_POSITION_L);               // 8번 모터의 현재 각도 설정 값 0~1023

                    //print("current degree 1, 2 : " + current_pos_1 + ", " + current_pos_2);
                    //print("goal    degree 1, 2 : " + GoalPos[6] + ", " + GoalPos[7]);

                    angular_distance1_left = Math.Abs(current_pos_1 - GoalPos[6]);                  // 목표 각도까지 남은 각도 값 저장
                    angular_distance2_left = Math.Abs(current_pos_2 - GoalPos[7]);                  // 목표 각도까지 남은 각도 값 저장

                    //print("left    degree 1, 2 : " + angular_distance1_left + ", " + angular_distance2_left);

                    if (angular_distance1_left > angular_distance2_left)
                    {
                        // cos 방법
                        velocity = (int)(((1 - Math.Cos((Math.PI * 2.0) *
                            (float)angular_distance1_left / (float)angular_distance1))) * 50) + 50;
                        //print("v1  : " + velocity);
                        dynamixel.dxl_write_word(7, P_GOAL_SPEED_L, velocity);
                        dynamixel.dxl_write_word(8, P_GOAL_SPEED_L, velocity);
                        most_left = angular_distance1_left;
                    }
                    else
                    {
                        // cos 방법
                        velocity = (int)(((1 - Math.Cos((Math.PI * 2.0) *
                            (float)angular_distance2_left / (float)angular_distance2))) * 50) + 50;
                        //print("v2  : " + velocity);
                        dynamixel.dxl_write_word(7, P_GOAL_SPEED_L, velocity);
                        dynamixel.dxl_write_word(8, P_GOAL_SPEED_L, velocity);
                        most_left = angular_distance2_left;
                    }
                    //print("m : " + most_left);
                    yield return new WaitForSeconds(0.1f);

                } while (most_left > 3);

                GoalPos[5] = 512 + 192;
                dynamixel.dxl_write_word(6, P_GOAL_POSITION_L, GoalPos[5]);

                yield return new WaitForSeconds(0.5f);

                // 여기 라인 시작 전 도착
                // 라인 그리기

                dis_x = segment_List_Left[i][1].getX() - segment_List_Left[i][0].getX();              // 라인의 X축 거리
                dis_y = segment_List_Left[i][1].getY() - segment_List_Left[i][0].getY();              // 라인의 Y축 거리
                distance = (int)Math.Sqrt((dis_x * dis_x) + (dis_y * dis_y));               // 두 점 사이의 거리 계산 (피타고라스)

                //print(segment_List[i][0].getX() + ", " + segment_List[i][0].getY());

                d_k = (float)move_pixel_unit / (float)distance;                             // 시작점과 끝점 거리에 따른 k증가값 계산
                //print(d_k);

                //print("m b : " + dynamixel.dxl_read_word(7, P_CURRENT_POSITION_L) + ", " + dynamixel.dxl_read_word(8, P_CURRENT_POSITION_L));

                for (k = 0f; k < 1f + d_k; k += d_k)                                             // 라인따라 k_d씩 이동
                {
                    xLeft = segment_List_Left[i][0].getX()
                        + k * (segment_List_Left[i][1].getX() - segment_List_Left[i][0].getX());      // 중간점의 x축 픽셀 좌표 계산 0~639
                    yLeft = segment_List_Left[i][0].getY()
                        + k * (segment_List_Left[i][1].getY() - segment_List_Left[i][0].getY());      // 중간점의 y축 픽셀 좌표 계산 0~479

                    //print("L + D : " + xLeft + " , " + yLeft);

                    LocationX_Left = 14.4f - (float)(yLeft) * ((float)boundary_Height) / (float)height * 1.03f;               // 2픽셀 이동 후 종이 상의 X좌표 
                    LocationY_Left = 19.3f - (float)(xLeft - width * 0.5f) * ((float)boundary_width) / (float)width;  // 2픽셀 이동 후 종이 상의 X좌표

                    //print("L + D : " + LocationX_Left + " " + LocationY_Left);

                    thetas = inverse_Kinematic(LocationX_Left, LocationY_Left);
                    if (thetas != null)
                    {
                        GoalPos[6] = 512 + (int)(3.413 * 180 / Math.PI * thetas[0]);
                        GoalPos[7] = 512 - (int)(3.413 * 180 / Math.PI * thetas[1]);

                        dynamixel.dxl_write_word(7, P_GOAL_POSITION_L, GoalPos[6]);
                        dynamixel.dxl_write_word(8, P_GOAL_POSITION_L, GoalPos[7]);

                        //CommStatus = send_packet_with_array(P_GOAL_POSITION_L, GoalPos);
                        //get_result(CommStatus);

                        yield return new WaitForSeconds(0.2f);
                    }
                    else
                    {
                        segment_List_Right.Add(
                            new Point[] { new Point((int)xLeft, (int)yLeft), new Point(segment_List_Left[i][1].getX(), segment_List_Left[i][1].getY()) });
                        break;
                    }
                }

                //print("m a : " + dynamixel.dxl_read_word(7, P_CURRENT_POSITION_L) + ", " + dynamixel.dxl_read_word(8, P_CURRENT_POSITION_L));

                yield return new WaitForSeconds(0.1f);                                              // 기다림

                GoalPos[5] = 612;                                                                   // 올리는 각도
                dynamixel.dxl_write_word(6, P_GOAL_POSITION_L, GoalPos[5]);                         // 6번 모터에 올리는 각도 전송

                yield return new WaitForSeconds(0.3f);                                              // 기다림

                progress += (float)(1f / (float)segment_List_Left.Count * 100f);
                i++;
            }
            else
            {
                yield return new WaitForSeconds(0.5f);
            }
        }


        dynamixel.dxl_write_word(6, P_GOAL_POSITION_L, 612);
        dynamixel.dxl_write_word(7, P_GOAL_POSITION_L, 512);
        dynamixel.dxl_write_word(8, P_GOAL_POSITION_L, 512);

        yield return null;
    }

    double[] inverse_Kinematic(float x, float y)
    {
        double[] thetas = new double[2];

        if (Math.Sqrt(x * x + y * y) <= (l1 + l2 - 0.2))
        {
            double cos2 = ((x * x + y * y) - (l1 * l1 + l2 * l2)) / (2 * l1 * l2);
            double sin2 = Math.Sqrt(1 - cos2 * cos2);
            thetas[0] = Math.Atan2(sin2, cos2);

            double cos1 = ((l1 + l2 * cos2) * x + l2 * sin2 * y) / ((l1 + l2 * cos2) * (l1 + l2 * cos2) + (l2 * sin2) * (l2 * sin2));
            double sin1 = ((l1 + l2 * cos2) * y - l2 * sin2 * x) / ((l1 + l2 * cos2) * (l1 + l2 * cos2) + (l2 * sin2) * (l2 * sin2));
            thetas[1] = Math.Atan2(sin1, cos1);

            return thetas;
        }
        else
        {
            print("out of range");
            return null;
        }
    }

    public void initialize_motors()
    {
        if (dynamixel.dxl_initialize(portNumber, DEFAULT_BAUDNUM) == 0)
        {
            Debug.LogError("Failed to open USB2Dynamixel!");
            is_dynamixel_alive = false;
        }
        else
        {
            Debug.Log("Succeed to open USB2Dynamixel!");
            is_dynamixel_alive = true;
        }

        dynamixel.dxl_write_word(dynamixel.BROADCAST_ID, P_GOAL_SPEED_L, 300);
        dynamixel.dxl_write_word(dynamixel.BROADCAST_ID, P_TORQUE_LIMIT_L, 512 );

        GoalPos[0] = 512;
        GoalPos[1] = 412;
        GoalPos[2] = 512;
        GoalPos[3] = 512;
        GoalPos[4] = 512;
        GoalPos[5] = 612;
        GoalPos[6] = 512;
        GoalPos[7] = 512;

        CommStatus = send_packet_with_array(P_GOAL_POSITION_L, GoalPos);
        get_result(CommStatus);
    }

    int send_packet_with_array(int Data_Address, int[] data)
    {
        dynamixel.dxl_set_txpacket_id(dynamixel.BROADCAST_ID);
        dynamixel.dxl_set_txpacket_instruction(dynamixel.INST_SYNC_WRITE);
        dynamixel.dxl_set_txpacket_parameter(0, Data_Address);
        dynamixel.dxl_set_txpacket_parameter(1, 2);
        for (int i = 0; i < NUM_ACTUATOR; i++)
        {
            dynamixel.dxl_set_txpacket_parameter(2 + 3 * i, i + 1);
            dynamixel.dxl_set_txpacket_parameter(2 + 3 * i + 1, dynamixel.dxl_get_lowbyte(data[i]));
            dynamixel.dxl_set_txpacket_parameter(2 + 3 * i + 2, dynamixel.dxl_get_highbyte(data[i]));
        }
        dynamixel.dxl_set_txpacket_length((2 + 1) * NUM_ACTUATOR + 4);

        dynamixel.dxl_txrx_packet();

        return CommStatus = dynamixel.dxl_get_result();
    }

    void get_result(int CommStatus)
    {
        if (CommStatus == dynamixel.COMM_RXSUCCESS)
        {
            PrintErrorCode();
            is_dynamixel_alive = true;
        }
        else
        {
            PrintCommStatus(CommStatus);
            is_dynamixel_alive = false;
        }
    }

    static void PrintCommStatus(int CommStatus)
    {
        string log = string.Empty;

        switch (CommStatus)
        {
            case dynamixel.COMM_TXFAIL:
                log = "COMM_TXFAIL: Failed transmit instruction packet!";
                break;

            case dynamixel.COMM_TXERROR:
                log = "COMM_TXERROR: Incorrect instruction packet!";
                break;

            case dynamixel.COMM_RXFAIL:
                log = "COMM_RXFAIL: Failed get status packet from device!";
                break;

            case dynamixel.COMM_RXWAITING:
                log = "COMM_RXWAITING: Now recieving status packet!";
                break;

            case dynamixel.COMM_RXTIMEOUT:
                log = "COMM_RXTIMEOUT: There is no status packet!";
                break;

            case dynamixel.COMM_RXCORRUPT:
                log = "COMM_RXCORRUPT: Incorrect status packet!";
                break;

            default:
                log = "This is unknown error code!";
                break;
        }
        Debug.LogError(log);
    }

    static void PrintErrorCode()
    {
        string log = string.Empty;

        if (dynamixel.dxl_get_rxpacket_error(dynamixel.ERRBIT_VOLTAGE) == 1)
            log = "Input voltage error!";

        if (dynamixel.dxl_get_rxpacket_error(dynamixel.ERRBIT_ANGLE) == 1)
            log = "Angle limit error!";

        if (dynamixel.dxl_get_rxpacket_error(dynamixel.ERRBIT_OVERHEAT) == 1)
            log = "Overheat error!";

        if (dynamixel.dxl_get_rxpacket_error(dynamixel.ERRBIT_RANGE) == 1)
            log = "Out of range error!";

        if (dynamixel.dxl_get_rxpacket_error(dynamixel.ERRBIT_CHECKSUM) == 1)
            log = "Checksum error!";

        if (dynamixel.dxl_get_rxpacket_error(dynamixel.ERRBIT_OVERLOAD) == 1)
            log = "Overload error!";

        if (dynamixel.dxl_get_rxpacket_error(dynamixel.ERRBIT_INSTRUCTION) == 1)
            log = "Instruction code error!";

        if (!string.IsNullOrEmpty(log))
            Debug.Log(log);
    }
}
