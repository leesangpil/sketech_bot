using UnityEngine;
using System.Collections;

public class Cursor : MonoBehaviour {

    public Texture cursorImage;

	// Use this for initialization
	void Start () {
        Screen.showCursor = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnGUI()
    {
        Vector3 mousePos = Input.mousePosition;
        Rect pos = new Rect(mousePos.x - cursorImage.width * 0.1f, Screen.height - mousePos.y - cursorImage.height * 0.8f, cursorImage.width, cursorImage.height);
        GUI.Label(pos, cursorImage);
    }
}
