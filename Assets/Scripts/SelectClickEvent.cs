using UnityEngine;
using System.Collections;

public class SelectClickEvent : MonoBehaviour {

    public static int selectNum = 0;

    public Texture[] _texture;

    private bool _onbutton;
    private bool _clickbutton;
    private bool _onbutton1;
    private bool _clickbutton1;
    private bool _onbutton2;
    private bool _clickbutton2;
    private bool _onbutton3;
    private bool _clickbutton3;

    private float AxisXmin_b1;
    private float AxisXmax_b1;
    private float AxisYmin_b1;
    private float AxisYmax_b1;

    private float AxisXmin_b2;
    private float AxisXmax_b2;
    private float AxisYmin_b2;
    private float AxisYmax_b2;

    private float AxisXmin_b3;
    private float AxisXmax_b3;
    private float AxisYmin_b3;
    private float AxisYmax_b3;

    private float AxisXmin_b4;
    private float AxisXmax_b4;
    private float AxisYmin_b4;
    private float AxisYmax_b4;

	// Use this for initialization
	void Start () {

        _onbutton = false;
        _clickbutton = false;
        _onbutton1 = false;
        _clickbutton1 = false;
        _onbutton2 = false;
        _clickbutton2 = false;
        _onbutton3 = false;
        _clickbutton3 = false;
	}
	
	// Update is called once per frame
	void Update () {

        AxisXmin_b1 = GameObject.Find("Main Camera").camera.pixelWidth * 0.163f;
        AxisXmax_b1 = GameObject.Find("Main Camera").camera.pixelWidth * 0.356f;
        AxisYmin_b1 = GameObject.Find("Main Camera").camera.pixelHeight * 0.440f;
        AxisYmax_b1 = GameObject.Find("Main Camera").camera.pixelHeight * 0.502f;

        AxisXmin_b2 = GameObject.Find("Main Camera").camera.pixelWidth * 0.642f;
        AxisXmax_b2 = GameObject.Find("Main Camera").camera.pixelWidth * 0.837f;
        AxisYmin_b2 = GameObject.Find("Main Camera").camera.pixelHeight * 0.440f;
        AxisYmax_b2 = GameObject.Find("Main Camera").camera.pixelHeight * 0.502f;

        AxisXmin_b3 = GameObject.Find("Main Camera").camera.pixelWidth * 0.163f;
        AxisXmax_b3 = GameObject.Find("Main Camera").camera.pixelWidth * 0.356f;
        AxisYmin_b3 = GameObject.Find("Main Camera").camera.pixelHeight * 0.892f;
        AxisYmax_b3 = GameObject.Find("Main Camera").camera.pixelHeight * 0.957f;

        AxisXmin_b4 = GameObject.Find("Main Camera").camera.pixelWidth * 0.642f;
        AxisXmax_b4 = GameObject.Find("Main Camera").camera.pixelWidth * 0.837f;
        AxisYmin_b4 = GameObject.Find("Main Camera").camera.pixelHeight * 0.892f;
        AxisYmax_b4 = GameObject.Find("Main Camera").camera.pixelHeight * 0.957f;
	}

    void OnGUI()
    {
        Event e = Event.current;
        //print(e.mousePosition);
        //print(((int)GameObject.Find("Main Camera").camera.pixelWidth) + " " + ((int)GameObject.Find("Main Camera").camera.pixelHeight));
        if (GameObject.Find("Manager").GetComponent<SelectMode>().ButtonEnable)
        {
            // 버튼 1
            if ((e.mousePosition.x > AxisXmin_b1 && e.mousePosition.x < AxisXmax_b1) && (e.mousePosition.y > AxisYmin_b1 && e.mousePosition.y < AxisYmax_b1))
            {
                GameObject.Find("Button_Mode1").renderer.material.mainTexture = _texture[1];
                if (_onbutton == false)
                {
                    _onbutton = true;
                    GameObject.Find("AudioSource_ButtonOn").GetComponent<AudioSource>().Play();
                }

                if (Input.GetMouseButtonDown(0))    // 마우스 왼쪽 버튼이 클릭되면
                {
                    if (_clickbutton == false)     // 여기에 캡쳐가 안되면 안넘어가게끔 처리를 해 줘야함
                    {
                        _clickbutton = true;
                        GameObject.Find("AudioSource_ButtonClick").GetComponent<AudioSource>().Play();
                        selectNum = 1;
                        StartCoroutine("Delay");
                    }
                }
                _clickbutton = false;
            }
            else
            {
                _onbutton = false;
                GameObject.Find("Button_Mode1").renderer.material.mainTexture = _texture[0];
            }

            // 버튼 2
            if ((e.mousePosition.x > AxisXmin_b2 && e.mousePosition.x < AxisXmax_b2) && (e.mousePosition.y > AxisYmin_b2 && e.mousePosition.y < AxisYmax_b2))
            {
                GameObject.Find("Button_Mode2").renderer.material.mainTexture = _texture[3];
                if (_onbutton2 == false)
                {
                    _onbutton2 = true;
                    GameObject.Find("AudioSource_ButtonOn").GetComponent<AudioSource>().Play();
                }

                if (Input.GetMouseButtonDown(0))    // 마우스 왼쪽 버튼이 클릭되면
                {
                    if (_clickbutton2 == false)     // 여기에 캡쳐가 안되면 안넘어가게끔 처리를 해 줘야함
                    {
                        _clickbutton2 = true;
                        GameObject.Find("AudioSource_ButtonClick").GetComponent<AudioSource>().Play();
                        selectNum = 2;
                        StartCoroutine("Delay");
                    }
                }
                _clickbutton2 = false;
            }
            else
            {
                _onbutton2 = false;
                GameObject.Find("Button_Mode2").renderer.material.mainTexture = _texture[2];
            }

            // 버튼 3
            if ((e.mousePosition.x > AxisXmin_b3 && e.mousePosition.x < AxisXmax_b3) && (e.mousePosition.y > AxisYmin_b3 && e.mousePosition.y < AxisYmax_b3))
            {
                GameObject.Find("Button_Mode3").renderer.material.mainTexture = _texture[5];
                if (_onbutton1 == false)
                {
                    _onbutton1 = true;
                    GameObject.Find("AudioSource_ButtonOn").GetComponent<AudioSource>().Play();
                }

                if (Input.GetMouseButtonDown(0))    // 마우스 왼쪽 버튼이 클릭되면
                {
                    if (_clickbutton1 == false)     // 여기에 캡쳐가 안되면 안넘어가게끔 처리를 해 줘야함
                    {
                        _clickbutton1 = true;
                        GameObject.Find("AudioSource_ButtonClick").GetComponent<AudioSource>().Play();
                        selectNum = 3;
                        StartCoroutine("Delay");
                    }
                }
                _clickbutton1 = false;
            }
            else
            {
                _onbutton1 = false;
                GameObject.Find("Button_Mode3").renderer.material.mainTexture = _texture[4];
            }

            // 버튼 4
            if ((e.mousePosition.x > AxisXmin_b4 && e.mousePosition.x < AxisXmax_b4) && (e.mousePosition.y > AxisYmin_b4 && e.mousePosition.y < AxisYmax_b4))
            {
                GameObject.Find("Button_Mode4").renderer.material.mainTexture = _texture[7];
                if (_onbutton3 == false)
                {
                    _onbutton3 = true;
                    GameObject.Find("AudioSource_ButtonOn").GetComponent<AudioSource>().Play();
                }

                if (Input.GetMouseButtonDown(0))    // 마우스 왼쪽 버튼이 클릭되면
                {
                    if (_clickbutton3 == false)     // 여기에 캡쳐가 안되면 안넘어가게끔 처리를 해 줘야함
                    {
                        _clickbutton3 = true;
                        GameObject.Find("AudioSource_ButtonClick").GetComponent<AudioSource>().Play();
                        selectNum = 4;
                        StartCoroutine("Delay");
                    }
                }
                _clickbutton3 = false;
            }
            else
            {
                _onbutton3 = false;
                GameObject.Find("Button_Mode4").renderer.material.mainTexture = _texture[6];
            }
        }
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(0.5f);
        Application.LoadLevel("4.MainScene");
        yield return null;
    }
}
