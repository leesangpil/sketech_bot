using UnityEngine;
using System.Collections;

public class SelectMode : MonoBehaviour {

    public static Texture2D capture_shot;                  // 불러올 캡쳐된 이미지
    public static Texture2D gray_shot;                     // 흑백 이미지
    public static Texture2D canny_shot;                    // 캐니 엣지 이미지
    public static Texture2D line_trace_shot;               // 라인 트레이서 이미지
    public static Texture2D gray_mosaic_shot;              // 흑백 모자이크 이미지
    public static Texture2D abstract_line_shot;            // 추상적 선 이미지
    public static Texture2D hough_shot;                    // 허프 변환 이미지
    public static Texture2D division_hough_shot;           // 허프 변환 이미지
    public static Texture2D segment_trace_shot;            // 선분트래킹 이미지

    public int width;
    public int height;
    public bool ButtonEnable = false;

    public GameObject Manager;               // 프로그램 매니저

	// Use this for initialization
	void Start () {

        Manager = GameObject.Find("Manager");

        width = 640;
        height = 480;

        capture_shot = new Texture2D(width, height);              // 캡쳐
        gray_shot = new Texture2D(width, height);              // 흑백
        canny_shot = new Texture2D(width, height);              // 캐니 엣지 추출
        line_trace_shot = new Texture2D(width, height);              // 라인 트레이서
        gray_mosaic_shot = new Texture2D(width, height);              // 흑백 모자이크
        abstract_line_shot = new Texture2D(width, height);              // 추상적 선
        hough_shot = new Texture2D(width, height);              // 허프 변환 이미지
        division_hough_shot = new Texture2D(width, height);              // 허프 변환 이미지
        segment_trace_shot = new Texture2D(width, height);              // 선분트래킹 이미지

        Manager.GetComponent<ImageProcessing>().LoadedImage(capture_shot);  // 그림 불러오기

        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++)
                abstract_line_shot.SetPixel(i, j, Color.white);

        StartCoroutine(ImageProcess());
	}
	
	// Update is called once per frame
	void Update () {

        //  라인트레이싱된 그림
        //if (Input.GetKeyDown(KeyCode.Alpha1))
        //{
        //    Manager.GetComponent<ImageProcessing>().ToGray(capture_shot, gray_shot);
        //    Manager.GetComponent<ImageProcessing>().medium_filtering(gray_shot, gray_shot);
        //    Manager.GetComponent<ImageProcessing>().remainder(gray_shot);
        //    Manager.GetComponent<ImageProcessing>().canny_Edge_detection(gray_shot, canny_shot);
        //    Manager.GetComponent<ImageProcessing>().remainder(canny_shot);
        //    Manager.GetComponent<ImageProcessing>().lineTrace(canny_shot, line_trace_shot);
        //    GameObject.Find("Result_Place1").renderer.material.mainTexture = line_trace_shot;
        
        //    Manager.GetComponent<ImageProcessing>().mosaic(gray_shot, gray_mosaic_shot);
        //    Manager.GetComponent<ImageProcessing>().abstractLines(gray_mosaic_shot, abstract_line_shot);
        //    GameObject.Find("Result_Place2").renderer.material.mainTexture = abstract_line_shot;
        
        //    Manager.GetComponent<ImageProcessing>().segmentTrace(canny_shot, segment_trace_shot);
        //    GameObject.Find("Result_Place3").renderer.material.mainTexture = segment_trace_shot;
        
        //    Manager.GetComponent<ImageProcessing>().divisionHoughTransformWithList(canny_shot, division_hough_shot);
        //    GameObject.Find("Result_Place4").renderer.material.mainTexture = division_hough_shot;
        //}
	}

    IEnumerator ImageProcess()
    {
        ButtonEnable = false;

        yield return new WaitForSeconds(0.1f);

        Manager.GetComponent<ImageProcessing>().ToGray(capture_shot, gray_shot);
        Manager.GetComponent<ImageProcessing>().medium_filtering(gray_shot, gray_shot);
        Manager.GetComponent<ImageProcessing>().remainder(gray_shot);
        Manager.GetComponent<ImageProcessing>().canny_Edge_detection(gray_shot, canny_shot);
        Manager.GetComponent<ImageProcessing>().remainder(canny_shot);
        Manager.GetComponent<ImageProcessing>().lineTrace(canny_shot, line_trace_shot);
        GameObject.Find("Result_Place1").renderer.material.mainTexture = line_trace_shot;

        Manager.GetComponent<ImageProcessing>().mosaic(gray_shot, gray_mosaic_shot);
        Manager.GetComponent<ImageProcessing>().abstractLines(gray_mosaic_shot, abstract_line_shot);
        GameObject.Find("Result_Place2").renderer.material.mainTexture = abstract_line_shot;

        Manager.GetComponent<ImageProcessing>().segmentTrace(canny_shot, segment_trace_shot);
        GameObject.Find("Result_Place3").renderer.material.mainTexture = segment_trace_shot;

        Manager.GetComponent<ImageProcessing>().divisionHoughTransformWithList(canny_shot, division_hough_shot);
        GameObject.Find("Result_Place4").renderer.material.mainTexture = division_hough_shot;

        ButtonEnable = true;

        yield return null;
    }
}
