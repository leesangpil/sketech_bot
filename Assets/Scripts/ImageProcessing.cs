using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class Point
{
    private int x, y;
    public Point() { x = 0; y = 0; }
    public Point(int x, int y) { this.x = x; this.y = y; }

    public void setXY(int x, int y) { this.x = x; this.y = y; }
    public int getX() { return x; }
    public int getY() { return y; }
};

public class Window
{
    private Point point1, point2, point3, point4, point5, point6, point7, point8, point9, point10, point11, point12, point13, point14, point15, point16;
    public bool hasLine1, hasLine2, hasLine3, hasLine4, hasLine5, hasLine6, hasLine7, hasLine8;
    public Window(Point leftDown, Point rightUp)
    {
        point1 = new Point(leftDown.getX(), rightUp.getY() - 1);
        point2 = new Point(leftDown.getX() + (rightUp.getX() - leftDown.getX()) / 4, rightUp.getY() - 1);
        point3 = new Point(leftDown.getX() + (rightUp.getX() - leftDown.getX()) / 2, rightUp.getY() - 1);
        point4 = new Point(leftDown.getX() + (rightUp.getX() - leftDown.getX()) * 3 / 4, rightUp.getY() - 1);
        point5 = new Point(rightUp.getX() - 1, rightUp.getY() - 1);
        point6 = new Point(rightUp.getX() - 1, leftDown.getY() + (rightUp.getY() - leftDown.getY()) * 3 / 4);
        point7 = new Point(rightUp.getX() - 1, leftDown.getY() + (rightUp.getY() - leftDown.getY()) / 2);
        point8 = new Point(rightUp.getX() - 1, leftDown.getY() + (rightUp.getY() - leftDown.getY()) / 4);
        point9 = new Point(rightUp.getX() - 1, leftDown.getY());
        point10 = new Point(leftDown.getX() + (rightUp.getX() - leftDown.getX()) * 3 / 4, leftDown.getY());
        point11 = new Point(leftDown.getX() + (rightUp.getX() - leftDown.getX()) / 2, leftDown.getY());
        point12 = new Point(leftDown.getX() + (rightUp.getX() - leftDown.getX()) / 4, leftDown.getY());
        point13 = new Point(leftDown.getX(), leftDown.getY());
        point14 = new Point(leftDown.getX(), leftDown.getY() + (rightUp.getY() - leftDown.getY()) / 4);
        point15 = new Point(leftDown.getX(), leftDown.getY() + (rightUp.getY() - leftDown.getY()) / 2);
        point16 = new Point(leftDown.getX(), leftDown.getY() + (rightUp.getY() - leftDown.getY()) * 3 / 4);
        hasLine1 = false; hasLine2 = false; hasLine3 = false; hasLine4 = false; hasLine5 = false; hasLine6 = false; hasLine7 = false; hasLine8 = false;
    }

    public Point getP1() { return point1; }
    public Point getP2() { return point2; }
    public Point getP3() { return point3; }
    public Point getP4() { return point4; }
    public Point getP5() { return point5; }
    public Point getP6() { return point6; }
    public Point getP7() { return point7; }
    public Point getP8() { return point8; }
    public Point getP9() { return point9; }
    public Point getP10() { return point10; }
    public Point getP11() { return point11; }
    public Point getP12() { return point12; }
    public Point getP13() { return point13; }
    public Point getP14() { return point14; }
    public Point getP15() { return point15; }
    public Point getP16() { return point16; }

    public void makeLine1() { hasLine1 = true; }
    public void makeLine2() { hasLine2 = true; }
    public void makeLine3() { hasLine3 = true; }
    public void makeLine4() { hasLine4 = true; }
    public void makeLine5() { hasLine5 = true; }
    public void makeLine6() { hasLine6 = true; }
    public void makeLine7() { hasLine7 = true; }
    public void makeLine8() { hasLine8 = true; }
};

public class ImageProcessing : MonoBehaviour
{
    public GameObject ProgramManager;

    public int height;
    public int width;

    private int thres_division = 11;                         // 임계값
    private int thres_single = 40;
    public const int division_rate_for_hough = 20;            // 화면 분할
    public const int division_rate_for_mosaic = 40;          // 모자이크용 화면 분할 값

    private int canny_lower_threshold = 60;
    private int canny_upper_threshold = 200;

    public List<Point>[, , ,] GroupH;
    public List<Point[]> Group_Hough_Line_List;
    public static List<Point> Hough_Point_List_Right = new List<Point>();
    public static List<Point> Hough_Point_List_Left = new List<Point>();
    public static List<Point[]> Hough_Line_List_Right = new List<Point[]>();
    public static List<Point[]> Hough_Line_List_Left = new List<Point[]>();
    public static List<Point[]> Trace_Line_List_Right = new List<Point[]>();
    public static List<Point[]> Trace_Line_List_Left = new List<Point[]>();
    
    public static List<Point> Trace_Point_List_Right  = new List<Point>();
    public static List<Point> Trace_Point_List_Left = new List<Point>();
    public static List<Point[]> Mosaic_Line_List = new List<Point[]>();     // 선분들의 첫점과 끝점을 저장할 리스트
    
    private Color[] gray_Level;                                     // 흑과백,회색을 8개로 나눠 Color를 저장할 배열

    // Use this for initialization
    void Start()
    {
        ProgramManager = GameObject.Find("ProgramManager");

        Hough_Point_List_Right = new List<Point>();
        Hough_Point_List_Left = new List<Point>();
        Hough_Line_List_Right = new List<Point[]>();
        Hough_Line_List_Left = new List<Point[]>();
        Trace_Line_List_Right = new List<Point[]>();
        Trace_Line_List_Left = new List<Point[]>();
        Trace_Point_List_Right  = new List<Point>();
        Trace_Point_List_Left = new List<Point>();
        Mosaic_Line_List = new List<Point[]>();  

        gray_Level = new Color[8];

        height = GameObject.Find("Manager").GetComponent<SelectMode>().height;
        width = GameObject.Find("Manager").GetComponent<SelectMode>().width;

        GroupH = new List<Point>[
            division_rate_for_hough,
            division_rate_for_hough,
            360,
            (int)(Math.Sqrt((width / division_rate_for_hough) * (width / division_rate_for_hough) + (height / division_rate_for_hough) * (height / division_rate_for_hough)))];

        for (int i = 0; i < 8; i++)
        {
            gray_Level[i] = new Color((i * 32.0f) / 255.0f, (i * 32.0f) / 255.0f, (i * 32.0f) / 255.0f);    // 흑과백,흑백을 8개로 나눠 gray_Level배열에 Color를 저장
        }
    }

    // 이미지 불러오기
    public void LoadedImage(Texture2D output_image)
    {
        // file load
        System.IO.FileStream _fs = new System.IO.FileStream("capture.bmp", System.IO.FileMode.Open, System.IO.FileAccess.Read);
        System.IO.BinaryReader _br = new System.IO.BinaryReader(_fs);

        byte[] buffer = _br.ReadBytes(10000000);

        output_image.LoadImage(buffer);
    }

    // 사진을 흑백으로 변환
    public void ToGray(Texture2D input_texture, Texture2D gray_texture)
    {
        for (int i = 0; i < input_texture.width; i++)
        {
            for (int j = 0; j < input_texture.height; j++)
            {
                Color color = input_texture.GetPixel(i, j);				                                // 픽셀값을 읽어 옴 
                double brightness = 0.2989 * color.r + 0.5870 * color.g + 0.1140 * color.b;			    // 흑백 1 (0.3 , 0.59, 0.11)
                Color tmp_gray = new Color((float)brightness, (float)brightness, (float)brightness);	// 밝기가 구별된 흑백 색상
                gray_texture.SetPixel(i, j, tmp_gray);	                                                // 해당픽셀에 위에서 구한 색을 넣음
            }
        }
        gray_texture.Apply();
    }

    // 캐니 엣지 검출
    public void canny_Edge_detection(Texture2D input_texture, Texture2D canny_output_texture)
    {
        double[,] sobel_X_mask = {{ -1.0, 0.0, 1.0 }, 
                                  { -2.0, 0.0, 2.0 }, 
                                  { -1.0, 0.0, 1.0 }};                                              // 소벨마스크(수직성분검출용)
        double[,] sobel_Y_mask = {{ -1.0, -2.0, -1.0}, 
                                  { 0.0, 0.0, 0.0 }, 
                                  { 1.0, 2.0, 1.0 }};                                               // 소벨마스크(수평성분검출용)

        int[,] grayArray = new int[height, width];

        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++)
                grayArray[j, i] = (int)(input_texture.GetPixel(i, j).r * 255);                      // 0 ~ 1 값을 0 ~ 255 값으로 바꿔줌

        int[,] GaussArray = smoothingImage(grayArray);                                              // 가우시안 마스크로 스무딩하여 2차원배열에 저장
        int[,] SobelXArray = convolution(GaussArray, sobel_X_mask);                                 // 소벨마스크로 수직성분 검출하여 2차원배열에 저장
        int[,] SobelYArray = convolution(GaussArray, sobel_Y_mask);                                 // 소벨마스크로 수평성분 검출하여 2차원배열에 저장

        //int[,] SobelXYArray = convolutionXY(GaussArray, sobel_X_mask, sobel_Y_mask);
        double[,] angleArray = new double[height, width];                                           // 방향각을 저장할 2차원 배열
        double[,] magnitudeArray = new double[height, width];                                       // 기울기의 크기를 저장할 2차원 배열

        for (int y = 0; y < height; y++)
            for (int x = 0; x < width; x++)
            {
                angleArray[y, x] = Math.Atan2(SobelYArray[y, x], SobelXArray[y, x]);                                                // 방향각 계산
                magnitudeArray[y, x] = Math.Sqrt(SobelXArray[y, x] * SobelXArray[y, x] + SobelYArray[y, x] * SobelYArray[y, x]);    // 기울기의 크기 계산
            }
        int[,] NonMaxArray = CannyNonMaxDecision(angleArray, magnitudeArray);           // 기울기 크기가 방향각 방향의 주변값보다 작을 경우 ,엣지 대상에서 제거시킴
        int[,] ThresholdArray = convolveCannyThreshold(NonMaxArray, canny_lower_threshold, canny_upper_threshold);                  // 상한과 하한의 문턱치를 두어 상한을 초과하는 경우 무조건 엣지로 삼고,
        // 하한의 미만의 경우는 에지에서 제외시키며, 그 사이는 주변에 자신 이상인 것이 있을 때 엣지로 삼는다
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                float tmp_color_rate = (float)ThresholdArray[j, i] / 255f;                          // Color의 매개변수로 사용하기 위해 0~255값을 0~1로 바꿔줌
                Color temp_color = new Color(tmp_color_rate, tmp_color_rate, tmp_color_rate);       // 동일한 비율로 하나의 색을만듬
                canny_output_texture.SetPixel(i, j, temp_color);                                    // 만든색을 그림
            }
        }
        canny_output_texture.Apply();
    }
    

    // 스무딩을 위한 함수(마스크를 가우시안 마스크를 이용하여 스무딩)
    int[,] smoothingImage(int[,] Gray)//, double[,] Mask)
    {
        double[,] gaussian_mask = {{1.0/273.0,  4.0/273.0,  7.0/273.0,  4.0/273.0, 1.0/273.0}, 
                                   {4.0/273.0, 16.0/273.0, 16.0/273.0, 16.0/273.0, 4.0/273.0},
                                   {7.0/273.0, 26.0/273.0, 41.0/273.0, 26.0/273.0, 7.0/273.0}, 
                                   {4.0/273.0, 16.0/273.0, 16.0/273.0, 16.0/273.0, 4.0/273.0},
                                   {1.0/273.0,  4.0/273.0,  7.0/273.0,  4.0/273.0, 1.0/273.0}};     // 스무딩을 위한 가우시안 마스크

        int[,] R = new int[height, width];                  // 반환할 2차원 배열
        double sum;                                         // 누적할 값을 저장할 변수

        for (int y = 0; y < height - 4; y++)                // 그림의 마스크를 벗어나게 검출하지 않는 영역에서
            for (int x = 0; x < width - 4; x++)
            {
                sum = 0.0;
                for (int r = 0; r < 5; r++)                 // 마스크 크기안에서
                    for (int c = 0; c < 5; c++)
                        sum += Gray[y + r, x + c] * gaussian_mask[r, c];   // 마스크와 그림을 곱해서(컨볼루션하여) 누적하여 더함
                sum = Math.Abs(sum);                        // 부호를 고려하지 않은 컨볼루션
                R[y + 2, x + 2] = (int)sum;                 // (마스크가 5*5 사이즈 이므로) 위아래로 두칸을 띄고 위에서 컨볼루션 하여 더한값을 넣음
            }
        return R;                                           // 값 반환
    }

    //// 미분을 위한 컨볼루션(수평검출, 수직검출을 위해 소벨마스크 이용)
    int[,] convolution(int[,] Smoothing, double[,] Mask)
    {
        int[,] R = new int[height, width];                  // 반환할 2차원 배열
        double sum;                                         // 누적할 값을 저장할 변수

        for (int y = 0; y < height - 2; y++)                // 그림의 마스크를 벗어나게 검출하지 않는 영역에서
            for (int x = 0; x < width - 2; x++)
            {
                sum = 0.0;
                for (int r = 0; r < 3; r++)                 // 마스크 크기안에서
                    for (int c = 0; c < 3; c++)
                        sum += Smoothing[y + r, x + c] * Mask[r, c];   // 마스크와 그림을 곱해서(컨볼루션하여) 누적하여 더함
                R[y + 1, x + 1] = (int)sum;                 // (마스크가 3*3 사이즈 이므로) 위아래로 한칸을 띄고 위에서 컨볼루션 하여 더한값을 넣음
            }
        return R;                                           // 값 반환
    }


    //// 기울기 크기가 방향각 방향의 주변값보다 작을 경우 ,엣지 대상에서 제거시키는 함수
    int[,] CannyNonMaxDecision(double[,] Angle, double[,] Magnitude)
    {
        int[,] R = new int[height, width];                              // 반환할 2차원 배열
        double[] A = new double[9];                                     // AngleArray를 9개로 잘라 저장할 변수
        double[] M = new double[9];                                     // MagnitudeArray를 9개로 잘라 저장할 변수
        int index;                                                      // 인덱스 변수 (9를 세기 위함)

        for (int y = 0; y < height - 2; y++)                            // 그림의 마스크를 벗어나게 검출하지 않는 영역에서
            for (int x = 0; x < width - 2; x++)
            {
                index = 0;                                              // 인덱스 변수 초기화
                for (int r = 0; r < 3; r++)                             // 마스크 크기안에서
                    for (int c = 0; c < 3; c++)
                    {
                        A[index] = Angle[y + r, x + c];                 // AngleArray의 2차원 배열을 9개씩 잘라 1차원 배열로 만듬
                        M[index] = Magnitude[y + r, x + c];             // MagnitudeArray의 2차원 배열을 9개씩 잘라 1차원 배열로 만듬
                        index++;                                        // 인덱스 1씩 증가
                    }
                R[y + 1, x + 1] = nonMaxDecision(A, M);                 // 현재 픽셀의 엣지 강도가 가장 큰 값만 사용
            }
        return R;                                                       // 값 반환
    }

    //// 현재 픽셀의 엣지 강도가 가장 큰 값만 사용
    int nonMaxDecision(double[] A, double[] M)                      // A = AngleArray를 9개로 자른 1차원 배열
    {                                                               // G = MagnitudeArray를 9개로 자른 1차원 배열
        int mid = 4;                                                // 9개의 배열원소중 중앙값인 4
        double iAngle1, iAngle2, iAngle3;                           // 방향각1,2,3 (여기서 방향각이 4개가 아닌 이유는 위에서 Atan2를 사용하여 -180 ~ 180 으로 만들었기 때문)
        double range = Math.PI / 8.0;                               // 오차범위: -22.5도 ~ + 22.5도 (radian)

        for (int i = 0; i < mid; i++)
        {
            if (M[mid] > M[i] && M[mid] > M[8 - i])                 // 크기배열 9만큼 잘라서 1차원으로 만든 배열의 가운뎃 값이 앞의 4개 보다 크고
            {                                                       // 크기배열 9만큼 잘라서 1차원으로 만든 배열의 가운뎃 값이 뒤의 4개 보다 크면
                // (즉, 기울기 크기가 주변값 보다 클 경우)
                if (i == 3)                                                         // i = 3 일때는, 방향각이 세개가 모두필요
                {
                    iAngle1 = Math.PI;                                              // 방향각1 = PI
                    iAngle2 = 0;                                                    // 방향각2 = 0
                    iAngle3 = -Math.PI;                                             // 방향각3 = -PI
                    if ((A[mid] - range) < iAngle1 && iAngle1 < (A[mid] + range))       // iAngle1(PI)이 cAngle의 오차범위 내에 들어오면
                        return (int)M[mid];
                    else if ((A[mid] - range) < iAngle2 && iAngle2 < (A[mid] + range))  // iAngle2(0)이 cAngle의 오차범위 내에 들어오면
                        return (int)M[mid];
                    else if ((A[mid] - range) < iAngle3 && iAngle3 < (A[mid] + range))  // iAngle3(-PI)이 cAngle의 오차범위 내에 들어오면
                        return (int)M[mid];
                }
                else                                                                // i = 0,1,2 일때는, 방향각이 2개만 필요
                {
                    iAngle1 = Math.PI / 4.0 * (i + 1);                              // 방향각1 = PI/4, 2PI/4, 3PI/4
                    iAngle2 = Math.PI / 4.0 * i - 3.0 * Math.PI / 4.0;              // 방향각2 = -3PI/4, -2PI/4, -PI/4
                    if ((A[mid] - range) < iAngle1 && iAngle1 < (A[mid] + range))       // iAngle1(PI/4, 2PI/4, 3PI/4)이 cAngle의 오차범위 내에 들어오면
                        return (int)M[mid];
                    else if ((A[mid] - range) < iAngle2 && iAngle2 < (A[mid] + range))  // iAngle2(-3PI/4, -2PI/4, -PI/4)이 cAngle의 오차범위 내에 들어오면
                        return (int)M[mid];
                }
            }
        }                                                                           // 위의 경우중 하나라도 해당되면 엣지로 사용
        return 0;                                                                   // 위의 경우가 아닐때, 엣지에서 제외
    }

    //// 상한과 하한의 문턱치를 두어 상한을 초과하는 경우 무조건 엣지로 삼고,
    //// 하한의 미만의 경우는 에지에서 제외시키며, 그 사이는 주변에 자신 이상인 것이 있을 때 엣지로 삼는다.
    int[,] convolveCannyThreshold(int[,] NonMax, int lower, int upper)
    {
        int[,] R = new int[height, width];                                          // 반환할 2차원 배열
        int[] NM = new int[9];                                                      // NonMaxArray를 9개로 잘라 저장할 변수
        int index;                                                                  // 인덱스 변수(9를 세기위함)

        for (int y = 0; y < height - 2; y++)                                        // 그림의 마스크를 벗어나게 검출하지 않는 영역에서
            for (int x = 0; x < width - 2; x++)
            {
                index = 0;                                                          // 인덱스 변수 초기화
                for (int r = 0; r < 3; r++)                                         // 마스크 크기안에서
                    for (int c = 0; c < 3; c++)
                    {
                        NM[index] = NonMax[y + r, x + c];                           // NonMaxArray를 9개씩 잘라 1차원 배열로 저장
                        index++;                                                    // 인덱스 변수 1씩 증가
                    }
                R[y + 1, x + 1] = thresholdLimit(NM, lower, upper);                 // 9개로 잘라서 엣지 확인
            }
        return R;                                                                   // 값 반환
    }

    //// 상한과 하한의 문턱치를 두어 상한을 초과하는 경우 무조건 엣지로 삼고,
    //// 하한의 미만의 경우는 에지에서 제외시키며, 그 사이는 주변에 자신 이상인 것이 있을 때 엣지로 삼는다.
    int thresholdLimit(int[] NM, int lower, int upper)
    {
        int mid = 4;                            // 9개의 배열사이즈의 가운데 표시할 4

        if (NM[mid] > upper)                    // 9개 기울기값중 가운데값이 상한값 보다 크면
            return 0;                           // 엣지로 사용(black)
        else if (NM[mid] < lower)               // 9개 기울기값중 가운데값이 하한값 보다 작으면
            return 255;                         // 엣지에서 제외(white)
        else                                    // 9개 기울기값중 가운데값이 상한값 보다 작고, 하한값보다는 큰 경우
        {
            for (int i = 0; i < 9; i++)         // 8방향으로 확인하면서
            {
                if (i != 4)                      // 자기 자신을 제외하고
                    if (NM[i] >= NM[mid])           // 9개의 배열에서 주변값이 중앙값보다 크거나 같으면,
                        return 0;                   // 9개의 배열에서 중앙값을 엣지로 사용 (black)
            }
        }
        return 0;                               // 실질적인 엣지 제외가 아님 (함수 반환형을 맞춤)
    }
    
    // 라인 트레이스 함수
    public void lineTrace(Texture2D input_texture, Texture2D line_Trace_texture)
    {
        Stack<Point> point_Stack = new Stack<Point>();              // 점들을 저장할 스택

        Point here = new Point();                                   // 현재 위치를 저장할 점

        int r, c;                                                   // row, column index
        int branch;                                                 // 스택의 가지

        line_Trace_texture.SetPixels(input_texture.GetPixels());    // 그림 복사

        for (int i = 3; i < width - 2; i++)                         // 픽셀 영역에서
        {
            for (int j = 3; j < height - 2; j++)
            {
                if (line_Trace_texture.GetPixel(i, j) == Color.black)   // 검은 점 이면 (엣지이면)
                {
                    here.setXY(i, j);                                   // 현재좌표를 저장
                    if (i >= width * 0.5)                                // 현재 좌표값이 그림폭의 반보다 크거나 같으면
                        Trace_Point_List_Left.Add(here);                      // 왼팔로 그릴리스트에 점 추가 (첫 번째 점)
                    else                                                // 현재 좌표값이 그림폭의 반보다 작으면
                        Trace_Point_List_Right.Add(here);                     // 오른팔로 그릴리스트에 점 추가 (첫 번째 점)

                    while (true)                                         // 라인 각각을 트레이스 할 루프
                    {
                        branch = 0;                                     // 가지의 갯수 0으로 초기화 (새로운 가지의 시작)

                        r = here.getX();                                // 현재 위치의 x좌표
                        c = here.getY();                                // 현재 위치의 y좌표

                        line_Trace_texture.SetPixel(r, c, Color.blue);  // 그 위치를 파란색 점으로 바꿈 (따라가면서 바꿈)

                        // 누적하면서 가지의 갯수를 저장하고 스택에 점의 좌표를 저장시킴 (상,하,좌,우 검사) // 하좌우상

                        branch += push_loc(line_Trace_texture, ref point_Stack, r, c + 1);      // 상
                        branch += push_loc(line_Trace_texture, ref point_Stack, r, c - 1);      // 하
                        branch += push_loc(line_Trace_texture, ref point_Stack, r - 1, c);      // 좌
                        branch += push_loc(line_Trace_texture, ref point_Stack, r + 1, c);      // 우

                        if (branch == 0)                                // 상하좌우를 검사하고도 가지의 갯수가 0이면, 나머지 방향으로 검출
                        {
                            branch += push_loc(line_Trace_texture, ref point_Stack, r - 1, c - 1);  // 좌하 대각선
                            branch += push_loc(line_Trace_texture, ref point_Stack, r + 1, c - 1);  // 우하 대각선
                            branch += push_loc(line_Trace_texture, ref point_Stack, r - 1, c + 1);  // 좌상 대각선
                            branch += push_loc(line_Trace_texture, ref point_Stack, r + 1, c + 1);  // 우상 대각선
                        }

                        if (point_Stack.Count == 0)                     // 한 라인의 끝지점이면
                            break;                                      // 루프를 빠져나감 (파란점으로 바뀌지 않은 검은 점들을 검출하기 위하여)
                        else
                        {
                            here = point_Stack.Pop();                   // 팝

                            if (i >= width * 0.5)                       // 현재 좌표값이 그림폭의 반보다 크거나 같으면
                                Trace_Point_List_Left.Add(here);              // 왼팔로 그릴리스트에 점 추가 (두 번째 점 부터)
                            else                                        // 현재 좌표값이 그림폭의 반보다 작으면
                                Trace_Point_List_Right.Add(here);             // 오른팔로 그릴리스트에 점 추가 (두 번째 점 부터)
                        }
                    }       // while 문 끝
                    point_Stack.Clear();
                }
            }
        }
        line_Trace_texture.Apply();
    }

    // 위치를 스택에 저장 할 함수
    int push_loc(Texture2D input_texture, ref Stack<Point> s, int r, int c)
    {
        if (input_texture.GetPixel(r, c) == Color.black)  // (상, 하, 좌, 우)가 검은 픽셀 이면
        {
            Point tmp = new Point(r, c);
            s.Push(tmp);                    // 스택에 저장
            return 1;                       // 1을 리턴
        }
        else                                // 검은 픽셀이 아니면
            return 0;                       // 0을 리턴
    }

    // 중간값 필터
    public void medium_filtering(Texture2D input_texture, Texture2D output_texture)
    {
        int[,] grayArray = new int[height, width];

        for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++)
                grayArray[j, i] = (int)(input_texture.GetPixel(i, j).r * 255);

        int[,] ResultArray = convolveMedian(grayArray);

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j++)
            {
                float tmp_color_rate = (float)ResultArray[j, i] / 255f;                             // Color의 매개변수로 사용하기 위해 0~255값을 0~1로 바꿔줌
                Color temp_color = new Color(tmp_color_rate, tmp_color_rate, tmp_color_rate);       // 동일한 비율로 하나의 색을만듬
                output_texture.SetPixel(i, j, temp_color);                                          // 만든색을 그림
            }
        }
        output_texture.Apply();
    }

    //public void reverseImage(Texture2D input_texture, Texture2D Reversed_texture)
    //{

    //    for (int i = 0; i < input_texture.width; i++)
    //    {
    //        for (int j = 0; j < input_texture.height; j++)
    //        {
    //            float reversed_rate = 1f - input_texture.GetPixel(i, j).r;                      // 색을 뒤집어 줌
    //            Color reversed_color = new Color(reversed_rate, reversed_rate, reversed_rate);  // 동일한 비율로 하나의 색을만듬
    //            Reversed_texture.SetPixel(i, j, reversed_color);                                // 만든색을 그림
    //        }
    //    }
    //    Reversed_texture.Apply();
    //}

    int[,] convolveMedian(int[,] GrayArray)
    {
        int[,] R = new int[height, width];
        int winSize = 9;
        int[] target = new int[winSize];

        for (int y = 0; y < height - 2; y++)
            for (int x = 0; x < width - 2; x++)
            {
                int index = 0;
                for (int r = 0; r < 3; r++)
                    for (int c = 0; c < 3; c++)
                    {
                        target[index] = GrayArray[y + r, x + c];
                        index++;
                    }
                //R[y + 1, x + 1] = selection_sort(target, winSize);  // 선택 정렬
                R[y + 1, x + 1] = quick_sort(target, 0, 8);  // 퀵 정렬
            }
        return R;
    }

    // 선택 정렬
    //int selection_sort(int[] target, int tsize)
    //{
    //    for(int i = 0; i < tsize - 1; i++)
    //        for (int j = i + 1; j < tsize; j++)
    //            if (target[i] > target[j])
    //                swap(ref target[i], ref target[j]);
    //    return (target[tsize / 2]);
    //}

    // swap 함수
    void swap(ref int a, ref int b)
    {
        int temp = a;
        a = b;
        b = temp;
    }

    // 퀵 소트
    int quick_sort(int[] arr, int left, int right)
    {
        if (right > left)
        {
            int pivot = arr[left], temp = left;
            for (int i = left + 1; i <= right; i++)
                if (arr[i] < pivot)
                {
                    temp++;
                    swap(ref arr[i], ref arr[temp]);
                }
            pivot = temp;
            swap(ref arr[left], ref arr[pivot]);

            quick_sort(arr, left, pivot - 1);
            quick_sort(arr, pivot + 1, right);
        }
        return (arr[4]);
    }

    // 그림의 나머지 영역 처리하는 함수
    public void remainder(Texture2D shot)
    {
        for (int w = 0; w < width; w++)
        {
            for (int h = 0; h < 3; h++)
            {
                shot.SetPixel(w, h, Color.white);
                shot.SetPixel(w, height - 1 - h, Color.white);
            }
        }
        for (int w = 0; w < 3; w++)
        {
            for (int h = 0; h < height; h++)
            {
                shot.SetPixel(w, h, Color.white);
                shot.SetPixel(width - 1 - w, h, Color.white);
            }
        }
        shot.Apply();
    }
    // 윈도우 구분선 그리는 함수
    public void grid(Texture2D input_texture, int winSize)
    {
        for (int i = 0; i < width; i = i + width / winSize)         // input_texture를 가로로 winSize만큼
        {
            for (int j = 0; j < height; j++)
            {
                input_texture.SetPixel(i, j, Color.black);          // 구분선을 검정색으로 그림
            }
        }
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < height; j = j + height / winSize)   // input_texture를 세로로 winSize만큼
            {
                input_texture.SetPixel(i, j, Color.black);          // 구분선을 검정색으로 그림
            }
        }
        input_texture.Apply();
    }
    // 각각의 윈도우에서 흑백밝기를 평균내고 평균값이 일정범위 내에 있으면 그 색으로 texture 칠하는 함수
    void average(Texture2D input_texture, Texture2D output_texture, int winSize)
    {
        float[,] avg = new float[winSize, winSize];     // 각각 윈도우의 흑백밝기 평균값이 들어갈 2차원 배열
        int _width = width / winSize;                   // 윈도우의 폭
        int _height = height / winSize;                 // 윈도우의 높이

        for (int i = 0; i < winSize; i++)               // 윈도우 크기만큼 돌면서
        {
            for (int j = 0; j < winSize; j++)
            {
                float sum = 0;                          // sum변수 선언, 0으로 초기화
                for (int k = 0; k < _width; k++)        // 윈도우의 픽셀갯수만큼 돌면서
                {
                    for (int m = 0; m < _height; m++)
                    {
                        Color get_color = input_texture.GetPixel(i * _width + k, j * _height + m);  // 각 픽셀의 색을 얻음
                        sum += get_color.r;             // 색의 밝기를 sum에 누적하여 저장
                    }
                }
                avg[i, j] = sum / (_width * _height);   // sum을 윈도우의 픽셀갯수만큼 나눠 평균값을 구함
            }
        }
        for (int i = 0; i < winSize; i++)               // 윈도우 크기만큼 돌면서
            for (int j = 0; j < winSize; j++)
            {
                for (int g = 0; g < 8; g++)             // 8개만큼 돌면서
                {
                    if (avg[i, j] * 255 >= g * 32 && avg[i, j] * 255 < (g + 1) * 32)                        // 각 윈도우의 밝기 평균값이 해당범위내에 있으면
                    {
                        for (int k = 0; k < _width; k++)
                            for (int m = 0; m < _height; m++)
                                output_texture.SetPixel(i * _width + k, j * _height + m, gray_Level[g]);    // 해당 윈도우를 gray_Level[g]색으로 칠함
                    }
                }
            }
        output_texture.Apply();
    }
    // 모자이크 함수
    public void mosaic(Texture2D input_texture, Texture2D output_texture)
    {
        int[,] grayArray = new int[height, width];          // texture의 밝기값을 저장할 2차원 배열
        Texture2D temp_shot = new Texture2D(width, height); // 스무딩한 이미지를 저장할 Texture2D

        for (int i = 0; i < width; i++)                     // 이미지의 크기만큼 돌면서
        {
            for (int j = 0; j < height; j++)
            {
                grayArray[j, i] = (int)(input_texture.GetPixel(i, j).r * 255);      // input_texture의 밝기값을 저장 ( 0 ~ 1 값을 0 ~ 255 값으로 바꿔 저장)
            }
        }

        int[,] smoothingArray = smoothingImage(grayArray);  // 이미지를 스무딩하여 2차원 배열에 저장
        for (int i = 0; i < width; i++)                     // 이미지 크기만큼 돌면서
        {
            for (int j = 0; j < height; j++)
            {
                float tmp_color_rate = (float)smoothingArray[j, i] / 255f;                          // Color의 매개변수로 사용하기 위해 0~255값을 0~1로 바꿔줌
                Color temp_color = new Color(tmp_color_rate, tmp_color_rate, tmp_color_rate);       // 동일한 비율로 하나의 색을만듬
                temp_shot.SetPixel(i, j, temp_color);                                               // 만든색을 그림
            }
        }
        //temp_shot.Apply();
        remainder(temp_shot);   // 그림의 나머지 영역 처리
        average(temp_shot, output_texture, division_rate_for_mosaic);  // 스무딩한 이미지를 윈도우별로 해당하는 색을 칠함
    }
    // 추상적 선을 그리는 함수
    public void abstractLines(Texture2D input_texture, Texture2D output_texture)
    {
        int winSize = division_rate_for_mosaic;
        Window[,] win = new Window[winSize, winSize];       // 화면분할한 만큼 윈도우 생성
        for (int i = 0; i < winSize; i++)                   // 윈도우 갯수만큼 돌면서
            for (int j = 0; j < winSize; j++)
                win[i, j] = new Window(new Point((width / winSize) * i, (height / winSize) * j),
                                       new Point((width / winSize) * (i + 1), (height / winSize) * (j + 1)));               // 모든 윈도우에 해당 점들(P1~P16)을 저장해줌 (왼쪽아래점과 오른쪽위점을 넣어주어 계산함)

        for (int m = 0; m < winSize; m++)                   // 윈도우 갯수만큼 돌면서
            for (int n = 0; n < winSize; n++)
            {
                if (input_texture.GetPixel(win[m, n].getP3().getX(), win[m, n].getP7().getY()) != gray_Level[7] &&          // 윈도우의 밝기가 gray_Level[6],gray_Level[7]이 아니면 (윈도우의 밝기가 gray_Level[6],gray_Level[7]이면 선을 안그려줌)
                    input_texture.GetPixel(win[m, n].getP3().getX(), win[m, n].getP7().getY()) != gray_Level[6])            // 윈도우의 가운데 픽셀의 밝기를 읽어오기 위해 P3의 X좌표, P7의 Y좌표를 사용
                {
                    if (input_texture.GetPixel(win[m, n].getP3().getX(), win[m, n].getP7().getY()) == gray_Level[0])        // 윈도우의 밝기가 gray_Level[0]이면
                    {                                                                                                       // 해당 윈도우에
                        win[m, n].makeLine1();                                                                              // 라인1을 만듬
                        win[m, n].makeLine2();                                                                              // 라인2을 만듬
                        win[m, n].makeLine3();                                                                              // 라인3을 만듬
                        win[m, n].makeLine4();                                                                              // 라인4을 만듬
                        win[m, n].makeLine5();                                                                              // 라인5을 만듬
                        win[m, n].makeLine6();                                                                              // 라인6을 만듬
                        win[m, n].makeLine7();                                                                              // 라인7을 만듬
                        win[m, n].makeLine8();                                                                              // 라인8을 만듬
                    }
                    else if (input_texture.GetPixel(win[m, n].getP3().getX(), win[m, n].getP7().getY()) == gray_Level[1])   // 윈도우의 밝기가 gray_Level[1]이면
                    {                                                                                                       // 해당 윈도우에
                        win[m, n].makeLine1();                                                                              // 라인1을 만듬
                        win[m, n].makeLine2();                                                                              // 라인2을 만듬
                        win[m, n].makeLine3();                                                                              // 라인3을 만듬
                        win[m, n].makeLine4();                                                                              // 라인4을 만듬
                        win[m, n].makeLine5();                                                                              // 라인5을 만듬
                        win[m, n].makeLine6();                                                                              // 라인6을 만듬
                    }
                    else if (input_texture.GetPixel(win[m, n].getP3().getX(), win[m, n].getP7().getY()) == gray_Level[2])   // 윈도우의 밝기가 gray_Level[2]이면
                    {                                                                                                       // 해당 윈도우에
                        win[m, n].makeLine1();                                                                              // 라인1을 만듬
                        win[m, n].makeLine2();                                                                              // 라인2을 만듬
                        win[m, n].makeLine3();                                                                              // 라인3을 만듬
                        win[m, n].makeLine4();                                                                              // 라인4을 만듬
                    }
                    else if (input_texture.GetPixel(win[m, n].getP3().getX(), win[m, n].getP7().getY()) == gray_Level[3])   // 윈도우의 밝기가 gray_Level[3]이면
                    {                                                                                                       // 해당 윈도우에
                        win[m, n].makeLine1();                                                                              // 라인1을 만듬
                        win[m, n].makeLine2();                                                                              // 라인2을 만듬
                       // win[m, n].makeLine3();                                                                              // 라인3을 만듬
                    }
                    else if (input_texture.GetPixel(win[m, n].getP3().getX(), win[m, n].getP7().getY()) == gray_Level[4])   // 윈도우의 밝기가 gray_Level[4]이면
                    {                                                                                                       // 해당 윈도우에
                        win[m, n].makeLine1();                                                                              // 라인1을 만듬
                        //win[m, n].makeLine2();                                                                              // 라인2을 만듬
                    }
                    else if (input_texture.GetPixel(win[m, n].getP3().getX(), win[m, n].getP7().getY()) == gray_Level[5])   // 윈도우의 밝기가 gray_Level[5]이면
                    {                                                                                                       // 해당 윈도우에
                        //win[m, n].makeLine1();                                                                              // 라인1을 만듬
                    }
                }
            }

        Point first = new Point();      // 라인의 시작점을 저장할 Point변수
        Point last = new Point();       // 라인의 끝점을 저장할 Point변수
        int p = 0;                      // 인덱스로 사용할 변수

        for (int m = 0; m < winSize; m++)       // 윈도우 갯수만큼 돌면서
            for (int n = 0; n < winSize; n++)
            {
                while (true)                                                                                // Line1을 검출 ('/'대각선)
                {
                    if ((m + p) < winSize && (n + p) < winSize && win[m + p, n + p].hasLine1 == true)       // 해당 윈도우에 Line1이 있으면 ('/'대각선이므로 m으로 1씩증가, n으로 1씩 증가 하면서 검사함)
                    {                                                                                       // 윈도우의 범위를 벗어날수 있으므로 (m + p) < winSize 과 (n + p) < winSize 도 조건검사함
                        win[m + p, n + p].hasLine1 = false;                                                 // Line1을 false로 바꿈 - 다음에 중복하여 검출되는것을 막기위함
                        first.setXY(win[m, n].getP13().getX(), win[m, n].getP13().getY());                  // 시작점을 시작윈도우의 P13로 저장
                        last.setXY(win[m + p, n + p].getP5().getX(), win[m + p, n + p].getP5().getY());     // 마지막점을 해당윈도우의 P5로 저장
                        p++;                                                                                // p를 1씩 증가
                    }
                    else                                                                                    // 해당 윈도우에 Line1이 없으면
                    {
                        if (p > 0)                                                                          // p가 0이상이면 - 검출이 됐으면
                        {
                            makeLine_diagonal(output_texture, first, last);                                 // output_texture에 대각선을 그림
                        }
                        p = 0;                                                                              // p를 0으로 초기화
                        break;                                                                              // while문 빠져나감
                    }
                }

                while (true)                                                                                // Line2을 검출 ('＼'대각선)
                {
                    if ((m + p) < winSize && (n - p) >= 0 && win[m + p, n - p].hasLine2 == true)            // 해당 윈도우에 Line2이 있으면 ('＼'대각선이므로 m으로 1씩증가, n으로 1씩 감소 하면서 검사함)
                    {                                                                                       // 윈도우의 범위를 벗어날수 있으므로 (m + p) < winSize 과 (n - p) < winSize 도 조건검사함
                        win[m + p, n - p].hasLine2 = false;                                                 // Line2을 false로 바꿈 - 다음에 중복하여 검출되는것을 막기위함
                        first.setXY(win[m, n].getP1().getX(), win[m, n].getP1().getY());                    // 시작점을 시작윈도우의 P1로 저장
                        last.setXY(win[m + p, n - p].getP9().getX(), win[m + p, n - p].getP9().getY());     // 마지막점을 해당윈도우의 P9로 저장
                        p++;                                                                                // p를 1씩 증가
                    }
                    else                                                                                    // 해당 윈도우에 Line2이 없으면
                    {
                        if (p > 0)                                                                          // p가 0이상이면 - 검출이 됐으면
                        {
                            makeLine_diagonal(output_texture, first, last);                                 // output_texture에 대각선을 그림
                        }
                        p = 0;                                                                              // p를 0으로 초기화
                        break;                                                                              // while문 빠져나감
                    }
                }
                while (true)                                                                                // Line3을 검출 ('─'가로선)
                {
                    if ((m + p) < winSize && win[m + p, n].hasLine3 == true)                                // 해당 윈도우에 Line3이 있으면 ('─'가로선이므로 m으로 1씩증가 하면서 검사함)
                    {                                                                                       // 윈도우의 범위를 벗어날수 있으므로 (m + p) < winSize 도 조건검사함
                        win[m + p, n].hasLine3 = false;                                                     // Line3을 false로 바꿈 - 다음에 중복하여 검출되는것을 막기위함
                        first.setXY(win[m, n].getP15().getX(), win[m, n].getP15().getY());                  // 시작점을 시작윈도우의 P15로 저장
                        last.setXY(win[m + p, n].getP7().getX(), win[m + p, n].getP7().getY());             // 마지막점을 해당윈도우의 P7로 저장
                        p++;                                                                                // p를 1씩 증가
                    }
                    else                                                                                    // 해당 윈도우에 Line3이 없으면
                    {
                        if (p > 0)                                                                          // p가 0이상이면 - 검출이 됐으면
                        {
                            makeLine_horizontal(output_texture, first, last);                               // output_texture에 가로선을 그림
                        }
                        p = 0;                                                                              // p를 0으로 초기화
                        break;                                                                              // while문 빠져나감
                    }
                }
                while (true)                                                                                // Line4을 검출 ('│'세로선)
                {
                    if ((n + p) < winSize && win[m, n + p].hasLine4 == true)                                // 해당 윈도우에 Line4가 있으면 ('│'세로선이므로 n으로 1씩증가 하면서 검사함)
                    {                                                                                       // 윈도우의 범위를 벗어날수 있으므로 (n + p) < winSize 도 조건검사함
                        win[m, n + p].hasLine4 = false;                                                     // Line4을 false로 바꿈 - 다음에 중복하여 검출되는것을 막기위함
                        first.setXY(win[m, n].getP11().getX(), win[m, n].getP11().getY());                  // 시작점을 시작윈도우의 P11로 저장
                        last.setXY(win[m, n + p].getP3().getX(), win[m, n + p].getP3().getY());             // 마지막점을 해당윈도우의 P3로 저장
                        p++;                                                                                // p를 1씩 증가
                    }
                    else                                                                                    // 해당 윈도우에 Line4이 없으면
                    {
                        if (p > 0)                                                                          // p가 0이상이면 - 검출이 됐으면
                        {
                            makeLine_vertical(output_texture, first, last);                                 // output_texture에 세로선을 그림
                        }
                        p = 0;                                                                              // p를 0으로 초기화
                        break;                                                                              // while문 빠져나감
                    }
                }
                while (true)                                                                                // Line5을 검출 ('│  '왼쪽 세로선)
                {
                    if ((n + p) < winSize && win[m, n + p].hasLine5 == true)                                // 해당 윈도우에 Line5가 있으면 ('│  '세로선이므로 n으로 1씩증가 하면서 검사함)
                    {                                                                                       // 윈도우의 범위를 벗어날수 있으므로 (n + p) < winSize 도 조건검사함
                        win[m, n + p].hasLine5 = false;                                                     // Line5을 false로 바꿈 - 다음에 중복하여 검출되는것을 막기위함
                        first.setXY(win[m, n].getP12().getX(), win[m, n].getP12().getY());                  // 시작점을 시작윈도우의 P12로 저장
                        last.setXY(win[m, n + p].getP2().getX(), win[m, n + p].getP2().getY());             // 마지막점을 해당윈도우의 P2로 저장
                        p++;                                                                                // p를 1씩 증가
                    }
                    else                                                                                    // 해당 윈도우에 Line5가 없으면
                    {
                        if (p > 0)                                                                          // p가 0이상이면 - 검출이 됐으면
                        {
                            makeLine_vertical(output_texture, first, last);                                 // output_texture에 왼쪽 세로선을 그림
                        }
                        p = 0;                                                                              // p를 0으로 초기화
                        break;                                                                              // while문 빠져나감
                    }
                }
                while (true)                                                                                // Line6을 검출 ('  │'오른쪽 세로선)
                {
                    if ((n + p) < winSize && win[m, n + p].hasLine6 == true)                                // 해당 윈도우에 Line6이 있으면 ('  │'세로선이므로 n으로 1씩증가 하면서 검사함)
                    {
                        win[m, n + p].hasLine6 = false;                                                     // Line6을 false로 바꿈 - 다음에 중복하여 검출되는것을 막기위함
                        first.setXY(win[m, n].getP10().getX(), win[m, n].getP10().getY());                  // 시작점을 시작윈도우의 P10로 저장
                        last.setXY(win[m, n + p].getP4().getX(), win[m, n + p].getP4().getY());             // 마지막점을 해당윈도우의 P4로 저장
                        p++;                                                                                // p를 1씩 증가
                    }
                    else                                                                                    // 해당 윈도우에 Line6이 없으면
                    {
                        if (p > 0)                                                                          // p가 0이상이면 - 검출이 됐으면
                        {
                            makeLine_vertical(output_texture, first, last);                                 // output_texture에 오른쪽 세로선을 그림
                        }
                        p = 0;                                                                              // p를 0으로 초기화
                        break;                                                                              // while문 빠져나감
                    }
                }
                while (true)                                                                                // Line7을 검출 ('￣'위쪽 가로선)
                {
                    if ((m + p) < winSize && win[m + p, n].hasLine7 == true)                                // 해당 윈도우에 Line7이 있으면 ('￣'위쪽 가로선이므로 m으로 1씩증가 하면서 검사함)
                    {                                                                                       // 윈도우의 범위를 벗어날수 있으므로 (m + p) < winSize 도 조건검사함
                        win[m + p, n].hasLine7 = false;                                                     // Line7을 false로 바꿈 - 다음에 중복하여 검출되는것을 막기위함
                        first.setXY(win[m, n].getP16().getX(), win[m, n].getP16().getY());                  // 시작점을 시작윈도우의 P16로 저장
                        last.setXY(win[m + p, n].getP6().getX(), win[m + p, n].getP6().getY());             // 마지막점을 해당윈도우의 P6로 저장
                        p++;                                                                                // p를 1씩 증가
                    }
                    else                                                                                    // 해당 윈도우에 Line7이 없으면
                    {
                        if (p > 0)                                                                          // p가 0이상이면 - 검출이 됐으면
                        {
                            makeLine_horizontal(output_texture, first, last);                               // output_texture에 위쪽 가로선을 그림
                        }
                        p = 0;                                                                              // p를 0으로 초기화
                        break;                                                                              // while문 빠져나감
                    }
                }
                while (true)                                                                                // Line8을 검출 ('_'아래쪽 가로선)
                {
                    if ((m + p) < winSize && win[m + p, n].hasLine8 == true)                                // 해당 윈도우에 Line8이 있으면 ('_'아래쪽 가로선이므로 m으로 1씩증가 하면서 검사함)
                    {                                                                                       // 윈도우의 범위를 벗어날수 있으므로 (m + p) < winSize 도 조건검사함
                        win[m + p, n].hasLine8 = false;                                                     // Line8을 false로 바꿈 - 다음에 중복하여 검출되는것을 막기위함
                        first.setXY(win[m, n].getP14().getX(), win[m, n].getP14().getY());                  // 시작점을 시작윈도우의 P14로 저장
                        last.setXY(win[m + p, n].getP8().getX(), win[m + p, n].getP8().getY());             // 마지막점을 해당윈도우의 P8로 저장
                        p++;                                                                                // p를 1씩 증가
                    }
                    else                                                                                    // 해당 윈도우에 Line8이 없으면
                    {
                        if (p > 0)                                                                          // p가 0이상이면 - 검출이 됐으면
                        {
                            makeLine_horizontal(output_texture, first, last);                               // output_texture에 위쪽 가로선을 그림
                        }
                        p = 0;                                                                              // p를 0으로 초기화
                        break;                                                                              // while문 빠져나감
                    }
                }
            }
        output_texture.Apply();
    }
    // 대각선 그리고 리스트에 라인의 시작점과 끝점을 저장하는 함수
    public void makeLine_diagonal(Texture2D temp, Point first, Point last)
    {
        int dis_x = last.getX() - first.getX();                             // 두 점 사이의 x좌표상의 거리 차
        int dis_y = last.getY() - first.getY();                             // 두 점 사이의 y좌표상의 거리 차
        int distance = (int)Math.Sqrt((dis_x * dis_x) + (dis_y * dis_y));   // 두 점 사이의 거리 계산 (피타고라스)

        float d_k = 1f / (float)distance;                                   // 두 점 사이의 거리의 역수

        for (float k = 0; k < 1 + d_k; k += d_k)                            // 두 점 사이의 거리의 역수만큼 증가하면서
        {
            temp.SetPixel(first.getX() + (int)(k * (last.getX() - first.getX())),
                first.getY() + (int)(k * (last.getY() - first.getY())), Color.black);    // 시작점과 끝점을 이어 검은색으로 그림
        }
        
        Mosaic_Line_List.Add(new Point[] { new Point(first.getX(), first.getY()), new Point(last.getX(), last.getY()) });     // 리스트에 시작점과 끝점을 저장
        
    }
    // 가로선을 그리고 리스트에 라인의 시작점과 끝점을 저장하는 함수
    public void makeLine_horizontal(Texture2D temp, Point first, Point last)
    {
        for (int k = 0; k < last.getX() - first.getX(); k++)                // 시작점과 끝점의 X좌표 차이만큼 돌면서
        {
            temp.SetPixel(first.getX() + k, first.getY(), Color.black);      // 가로선을 검은색으로 그림
        }
        Mosaic_Line_List.Add(new Point[] { new Point(first.getX(), first.getY()), new Point(last.getX(), last.getY()) });     // 리스트에 시작점과 끝점을 저장
    }
    // 세로선을 그리고 리스트에 라인의 시작점과 끝점을 저장하는 함수
    public void makeLine_vertical(Texture2D temp, Point first, Point last)
    {
        for (int k = 0; k < last.getY() - first.getY(); k++)                // 시작점과 끝점의 Y좌표 차이만큼 돌면서
        {
            temp.SetPixel(first.getX(), first.getY() + k, Color.black);      // 세로선을 검은색으로 그램
        }
        Mosaic_Line_List.Add(new Point[] { new Point(first.getX(), first.getY()), new Point(last.getX(), last.getY()) });     // 리스트에 시작점과 끝점을 저장
    }


    // 선분 트레이스 함수
    public void segmentTrace(Texture2D input_texture, Texture2D segment_Trace_texture)
    {
        List<Point[]> pointArrayList1 = new List<Point[]>();             // 로봇에게 실질적으로 보내는 선분의 처음과 끝점들의 집합
        Stack<Point> pointStack = new Stack<Point>();                   // 점들을 저장할 스택
        Point first = new Point();                                      // 첫 점을 저장할 포인트 버퍼
        Point last = new Point();                                       // 끝 점을 저장할 포인트 버퍼
        List<int> iAngleList = new List<int>();                         // 팝한 방향각만 저장할 리스트

        Point here = new Point();                                       // 현재 위치를 저장할 점
        int before_iAngle = 0;                                          // 이전에 방향값을 저장해 놓을 버퍼

        int r, c;                                                       // row, column index

        segment_Trace_texture.SetPixels(input_texture.GetPixels());     // 그림 복사

        for (int i = 3; i < width - 2; i++)                             // 픽셀 영역에서
        {
            for (int j = 3; j < height - 2; j++)
            {
                if (segment_Trace_texture.GetPixel(i, j) == Color.black)    // 검은 점 이면 (엣지이면)
                {
                    here.setXY(i, j);                                       // 현재좌표를 저장
                    first = here;                                           // 처음점은 무조건 첫점 버퍼에 넣음

                    while (true)                                         // 라인 각각을 트레이스 할 루프
                    {
                        r = here.getX();                                // 현재 위치의 x좌표
                        c = here.getY();                                // 현재 위치의 y좌표

                        segment_Trace_texture.SetPixel(r, c, Color.white);  // 그 위치를 파란색 점으로 바꿈 (따라가면서 바꿈)

                        // 누적하면서 가지의 갯수를 저장하고 스택에 점의 좌표를 저장시킴 (상,하,좌,우 검사) // 하좌우상

                        push_check(segment_Trace_texture, ref pointStack, ref iAngleList, r, c + 1, 1);      // 상
                        push_check(segment_Trace_texture, ref pointStack, ref iAngleList, r, c - 1, 2);      // 하
                        push_check(segment_Trace_texture, ref pointStack, ref iAngleList, r - 1, c, 3);      // 좌
                        push_check(segment_Trace_texture, ref pointStack, ref iAngleList, r + 1, c, 4);      // 우
                        push_check(segment_Trace_texture, ref pointStack, ref iAngleList, r - 1, c - 1, 5);  // 좌하 대각선
                        push_check(segment_Trace_texture, ref pointStack, ref iAngleList, r + 1, c - 1, 6);  // 우하 대각선
                        push_check(segment_Trace_texture, ref pointStack, ref iAngleList, r - 1, c + 1, 7);  // 좌상 대각선
                        push_check(segment_Trace_texture, ref pointStack, ref iAngleList, r + 1, c + 1, 8);  // 우상 대각선


                        if (pointStack.Count == 0)                      // 한 라인의 끝지점이면
                        {
                            if (before_iAngle != 0)
                            {
                                last = here;

                                pointArrayList1.Add(new Point[] { new Point(first.getX(), first.getY()), new Point(last.getX(), last.getY()) });
                                //pointArrayList1.Add(new Point[] { first, last });
                            }
                            before_iAngle = 0;
                            break;                                      // 루프를 빠져나감 (파란점으로 바뀌지 않은 검은 점들을 검출하기 위하여)
                        }
                        else                                            // 스택에 하나라도 들어가 있으면
                        {
                            here = pointStack.Pop();                    // 팝
                            if (before_iAngle == 0)
                            {
                                last = here;
                                before_iAngle = iAngleList[iAngleList.Count - 1];
                            }
                            else
                            {
                                if (before_iAngle == 8)
                                    if (iAngleList[iAngleList.Count - 1] == 8 || iAngleList[iAngleList.Count - 1] == 1 || iAngleList[iAngleList.Count - 1] == 4)
                                    {
                                        last = here;
                                        before_iAngle = iAngleList[iAngleList.Count - 1];
                                    }
                                    else
                                    {
                                        pointArrayList1.Add(new Point[] { new Point(first.getX(), first.getY()), new Point(last.getX(), last.getY()) });
                                        first = here;
                                        before_iAngle = 0;
                                    }
                                else if (before_iAngle == 7)
                                    if (iAngleList[iAngleList.Count - 1] == 7 || iAngleList[iAngleList.Count - 1] == 1 || iAngleList[iAngleList.Count - 1] == 3)
                                    {
                                        last = here;
                                        before_iAngle = iAngleList[iAngleList.Count - 1];
                                    }
                                    else
                                    {
                                        pointArrayList1.Add(new Point[] { new Point(first.getX(), first.getY()), new Point(last.getX(), last.getY()) });
                                        first = here;
                                        before_iAngle = 0;
                                    }
                                else if (before_iAngle == 6)
                                    if (iAngleList[iAngleList.Count - 1] == 6 || iAngleList[iAngleList.Count - 1] == 2 || iAngleList[iAngleList.Count - 1] == 4)
                                    {
                                        last = here;
                                        before_iAngle = iAngleList[iAngleList.Count - 1];
                                    }
                                    else
                                    {
                                        pointArrayList1.Add(new Point[] { new Point(first.getX(), first.getY()), new Point(last.getX(), last.getY()) });
                                        first = here;
                                        before_iAngle = 0;
                                    }
                                else if (before_iAngle == 5)
                                    if (iAngleList[iAngleList.Count - 1] == 5 || iAngleList[iAngleList.Count - 1] == 2 || iAngleList[iAngleList.Count - 1] == 3)
                                    {
                                        last = here;
                                        before_iAngle = iAngleList[iAngleList.Count - 1];
                                    }
                                    else
                                    {
                                        pointArrayList1.Add(new Point[] { new Point(first.getX(), first.getY()), new Point(last.getX(), last.getY()) });
                                        first = here;
                                        before_iAngle = 0;
                                    }
                                else if (before_iAngle == 4)
                                    if (iAngleList[iAngleList.Count - 1] == 4 || iAngleList[iAngleList.Count - 1] == 6 || iAngleList[iAngleList.Count - 1] == 8)
                                    {
                                        last = here;
                                        before_iAngle = iAngleList[iAngleList.Count - 1];
                                    }
                                    else
                                    {
                                        pointArrayList1.Add(new Point[] { new Point(first.getX(), first.getY()), new Point(last.getX(), last.getY()) });
                                        first = here;
                                        before_iAngle = 0;
                                    }
                                else if (before_iAngle == 3)
                                    if (iAngleList[iAngleList.Count - 1] == 3 || iAngleList[iAngleList.Count - 1] == 5 || iAngleList[iAngleList.Count - 1] == 7)
                                    {
                                        last = here;
                                        before_iAngle = iAngleList[iAngleList.Count - 1];
                                    }
                                    else
                                    {
                                        pointArrayList1.Add(new Point[] { new Point(first.getX(), first.getY()), new Point(last.getX(), last.getY()) });
                                        first = here;
                                        before_iAngle = 0;
                                    }
                                else if (before_iAngle == 2)
                                    if (iAngleList[iAngleList.Count - 1] == 2 || iAngleList[iAngleList.Count - 1] == 5 || iAngleList[iAngleList.Count - 1] == 6)
                                    {
                                        last = here;
                                        before_iAngle = iAngleList[iAngleList.Count - 1];
                                    }
                                    else
                                    {
                                        pointArrayList1.Add(new Point[] { new Point(first.getX(), first.getY()), new Point(last.getX(), last.getY()) });
                                        first = here;
                                        before_iAngle = 0;
                                    }
                                else if (before_iAngle == 1)
                                    if (iAngleList[iAngleList.Count - 1] == 1 || iAngleList[iAngleList.Count - 1] == 7 || iAngleList[iAngleList.Count - 1] == 8)
                                    {
                                        last = here;
                                        before_iAngle = iAngleList[iAngleList.Count - 1];
                                    }
                                    else
                                    {
                                        pointArrayList1.Add(new Point[] { new Point(first.getX(), first.getY()), new Point(last.getX(), last.getY()) });
                                        first = here;
                                        before_iAngle = 0;
                                    }
                            }
                        }
                        iAngleList.Clear();
                        pointStack.Clear();
                    }       // while 문 끝
                }
            }
        }
        int dis_x, dis_y, distance;
        for (int i = 0; i < pointArrayList1.Count; i++)
        {
            dis_x = pointArrayList1[i][1].getX() - pointArrayList1[i][0].getX();
            dis_y = pointArrayList1[i][1].getY() - pointArrayList1[i][0].getY();
            distance = (int)Math.Sqrt((dis_x * dis_x) + (dis_y * dis_y));

            if (distance < 5)
            {
                pointArrayList1.RemoveAt(i);
                i--;
            }
            else
                makeSegment(segment_Trace_texture, pointArrayList1[i][0], pointArrayList1[i][1]);
        }
        segment_Trace_texture.Apply();

        for (int i = 0; i < pointArrayList1.Count; i++ )
        {
            if (pointArrayList1[i][0].getX() >= width / 2)
                Trace_Line_List_Left.Add(new Point[] { new Point(pointArrayList1[i][0].getX(), pointArrayList1[i][0].getY()),
                    new Point(pointArrayList1[i][1].getX(), pointArrayList1[i][1].getY()) });
            else
                Trace_Line_List_Right.Add(new Point[] { new Point(pointArrayList1[i][0].getX(), pointArrayList1[i][0].getY()),
                    new Point(pointArrayList1[i][1].getX(), pointArrayList1[i][1].getY()) });
        }
    }
    // 위치를 스택에 저장 할 함수
    void push_check(Texture2D input_texture, ref Stack<Point> s, ref List<int> l, int r, int c, int i)
    {
        if (input_texture.GetPixel(r, c) == Color.black)  // (상, 하, 좌, 우)가 검은 픽셀 이면
        {
            Point tmp = new Point(r, c);
            s.Push(tmp);                    // 스택에 저장
            l.Add(i);
        }
    }
    // 대각선 그리고 리스트에 라인의 시작점과 끝점을 저장하는 함수
    public void makeSegment(Texture2D temp, Point first, Point last)
    {
        int dis_x = last.getX() - first.getX();                             // 두 점 사이의 x좌표상의 거리 차
        int dis_y = last.getY() - first.getY();                             // 두 점 사이의 y좌표상의 거리 차
        int distance = (int)Math.Sqrt((dis_x * dis_x) + (dis_y * dis_y));   // 두 점 사이의 거리 계산 (피타고라스)

        float d_k = 1f / (float)distance;                                   // 두 점 사이의 거리의 역수

        for (float k = 0; k < 1 + d_k; k += d_k)                            // 두 점 사이의 거리의 역수만큼 증가하면서
        {
            temp.SetPixel(first.getX() + (int)(k * (last.getX() - first.getX())),
                first.getY() + (int)(k * (last.getY() - first.getY())), Color.black);    // 시작점과 끝점을 이어 검은색으로 그림
        }
    }

    public void houghTransformWithGrouping(Texture2D input_texture, Texture2D hough_transformed_texture, int whereX, int whereY)
    {
        int _width = input_texture.width;                                       // 분할한 윈도우의 폭
        int _height = input_texture.height;                                     // 분할한 윈도우의 높이
        int diagonal = (int)Math.Sqrt((_width * _width) + (_height * _height));     // 빗변의 길이

        int X = (whereX * division_rate_for_hough / width);
        int Y = (whereY * division_rate_for_hough / height);

        int dot_count = 0;                                                    // 몇 번째 라인인지 검사할 변수
        
        for (int i = 0; i < 360; i++)
        {
            for (int j = 0; j < diagonal; j++)
            {
                GroupH[X, Y, i, j] = new List<Point>();                // 보팅결과를 저장할 2차원배열 초기화 (0)
            }
        }

        // (Look Up Table) -> 수행 속도 향상을 위해서 먼저 사인값과 코사인값을 넣어놓음
        float[] COS = new float[360];   // 0 ~ 360 까지의 라디안 값을 cos 값으로 저장할 배열
        float[] SIN = new float[360];   // 0 ~ 360 까지의 라디안 값을 sin 값으로 저장할 배열

        for (int theta = 0; theta < 360; theta++)
        {
            COS[theta] = (float)Math.Cos(theta * (Math.PI / 180.0d));      // 0도 부터 360도 까지의 cos 값을 미리 만들어 놓음
            SIN[theta] = (float)Math.Sin(theta * (Math.PI / 180.0d));      // 0도 부터 360도 까지의 sin 값을 미리 만들어 놓음
        }
        // 보팅
        for (int i = 0; i < _width; i++)                                   // 분할한 그림 사이즈 만큼 돌면서
        {
            for (int j = 0; j < _height; j++)
            {
                if (input_texture.GetPixel(i, j).r == 0)                   // 그림의 픽셀이 검정일때,
                {
                    dot_count++;
                    for (int theta = 0; theta < 360; theta++)
                    {
                        int d = (int)(i * COS[theta] + j * SIN[theta]);    // p = x*Cos(theta) + y*Sin(theta) 표현 (d = 원점과 직선사이의 거리)
                        if (d >= 0 && d < diagonal)                        // 원점과 직선사이의 거리가 0보다 크고, 그림의 최대 대각선 길이보다 짧으면,
                            GroupH[X, Y, theta, d].Add(new Point(whereX + i, whereY + j));                                 // 파라미터 공간에서 보팅(선 상에서의 점의 갯수 증가)
                    }
                }
            }
        }

        // 선 판별 (x,y 좌표가 선의 좌표들임)
        for (int d = 0; d < diagonal; d++)                                  // 대각선 길이만큼 돌면서,
        {
            for (int theta = 0; theta < 360; theta++)                       // 0 ~ 360도 까지
            {
                if (GroupH[X, Y, theta, d].Count > thres_division)                                   // 보팅된 값이 임계치값 보다 크면,
                {
                    if (theta >= 180 && d == 0)
                    {
                        if (GroupH[X, Y, theta - 180, d].Count > thres_division)
                        {
                            GroupH[X, Y, theta, d].Clear();
                        }
                    }
                }
            }
        }

        hough_transformed_texture.Apply();
    }

    public void houghTransform(Texture2D input_texture, Texture2D hough_transformed_texture)
    {
        int diagonal = (int)Math.Sqrt((width * width) + (height * height));     // 빗변의 길이
        int dot_count = 0;                                                    // 몇 번째 라인인지 검사할 변수

        Hough_Point_List_Right = new List<Point>();
        Hough_Point_List_Left = new List<Point>();
        List<Point>[,] GroupH = new List<Point>[360, diagonal];
        for (int i = 0; i < 360; i++)
        {
            for (int j = 0; j < diagonal; j++)
            {
                GroupH[i, j] = new List<Point>();                // 보팅결과를 저장할 2차원배열 초기화 (0)
            }
        }

        // (Look Up Table) -> 수행 속도 향상을 위해서 먼저 사인값과 코사인값을 넣어놓음
        float[] COS = new float[360];   // 0 ~ 360 까지의 라디안 값을 cos 값으로 저장할 배열
        float[] SIN = new float[360];   // 0 ~ 360 까지의 라디안 값을 sin 값으로 저장할 배열

        for (int theta = 0; theta < 360; theta++)
        {
            COS[theta] = (float)Math.Cos(theta * (Math.PI / 180.0d));      // 0도 부터 360도 까지의 cos 값을 미리 만들어 놓음
            SIN[theta] = (float)Math.Sin(theta * (Math.PI / 180.0d));      // 0도 부터 360도 까지의 sin 값을 미리 만들어 놓음
        }
        // 보팅
        for (int i = 0; i < width; i++)                                   // 분할한 그림 사이즈 만큼 돌면서
        {
            for (int j = 0; j < height; j++)
            {
                if (input_texture.GetPixel(i, j).r == 0)                   // 그림의 픽셀이 검정일때,
                {
                    dot_count++;
                    for (int theta = 0; theta < 360; theta++)
                    {
                        int d = (int)(i * COS[theta] + j * SIN[theta]);    // p = x*Cos(theta) + y*Sin(theta) 표현 (d = 원점과 직선사이의 거리)
                        if (d >= 0 && d < diagonal)                        // 원점과 직선사이의 거리가 0보다 크고, 그림의 최대 대각선 길이보다 짧으면,
                            GroupH[theta, d].Add(new Point(i, j));                                 // 파라미터 공간에서 보팅(선 상에서의 점의 갯수 증가)
                    }
                }
            }
        }

        // 선 판별 (x,y 좌표가 선의 좌표들임)
        for (int d = 0; d < diagonal; d++)                                  // 대각선 길이만큼 돌면서,
        {
            for (int theta = 0; theta < 360; theta++)                       // 0 ~ 360도 까지
            {
                if (GroupH[theta, d].Count > thres_single)                                   // 보팅된 값이 임계치값 보다 크면,
                {
                    if (theta >= 180 && d == 0)
                    {
                        if (GroupH[theta - 180, d].Count > thres_single)
                        {
                            GroupH[theta, d].Clear();
                        }
                    }
                    else
                    {
                        drawing_line_with_2dots(hough_transformed_texture, GroupH[theta, d][0], GroupH[theta, d][GroupH[theta, d].Count - 1]);
                        for (int k = 0; k < GroupH[theta, d].Count; k++)
                        {
                            //print(k + " : " + H[i, j][k].getX() + ", " + H[i, j][k].getY());
                            hough_transformed_texture.SetPixel(GroupH[theta, d][k].getX(), GroupH[theta, d][k].getY(), Color.red);
                            //Hough_Point_List.Add(GroupH[theta, d][k]);
                        }
                    }
                }
            }
        }



        hough_transformed_texture.Apply();
    }

    public void divisionHoughTransformWithList(Texture2D input_texture, Texture2D division_HT_texture)
    {
        Group_Hough_Line_List = new List<Point[]>();

        const float critical_value = 8f;

        int line_count = 0;
        int point_count = 0;
        int Group_count = 0;

        Point first_point = new Point();
        Point last_point = new Point();

        int dx1, dx2, dy1, dy2, dx, dy;
        int s1, s2, r1, r2, t1, t2, CrossX, CrossY;
        int BoundX_Min1, BoundX_Min2, BoundX_Max1, BoundX_Max2;
        int BoundY_Min1, BoundY_Min2, BoundY_Max1, BoundY_Max2;
        int mid_X1, mid_X2, mid_Y1, mid_Y2;
        float dis21, dis43, dis23, dis14;
        float k_d1, k_d2;

        double d_angle;
        float distance, distance1, distance2;
        float min_distance;
        float max_distance;

        int _width = width / division_rate_for_hough;                                 // 분할한 윈도우의 폭
        int _height = height / division_rate_for_hough;                               // 분할한 윈도우의 높이

        int diagonal = (int)Math.Sqrt((_width * _width) + (_height * _height));     // 빗변의 길이

        Texture2D unit_division_texture_B = new Texture2D(_width, _height);   // 윈도우 크기만큼 허프변환하여 저장할 변수
        Texture2D unit_division_texture_A = new Texture2D(_width, _height);   // 윈도우 크기만큼 허프변환하여 저장할 변수

        for (int i = 0; i < width; i += _width)                             // 윈도우의 폭만큼 증가시키면서
        {
            for (int j = 0; j < height; j += _height)                       // 윈도우의 높이만큼 증가시키면서
            {
                for (int l = 0; l < _width; l++)                            // 0부터 윈도우의 폭까지 증가시키면서
                {
                    for (int m = 0; m < _height; m++)                       // 0부터 윈도우의 높이까지 증가시키면서
                    {
                        unit_division_texture_B.SetPixel(l, m, input_texture.GetPixel(i + l, j + m));         // 윈도우 크기만큼 원본그림 복사
                        unit_division_texture_A.SetPixel(l, m, Color.white);
                    }
                }
                //unit_division_texture_B.Apply();

                houghTransformWithGrouping(unit_division_texture_B, unit_division_texture_A, i, j);                         // 윈도우 크기만큼 허프변환하여 반환
                //unit_division_texture_A.Apply();

                for (int l = 0; l < _width; l++)                            // 0부터 윈도우의 폭까지 증가시키면서
                {
                    for (int m = 0; m < _height; m++)                       // 0부터 윈도우의 높이까지 증가시키면서
                    {
                        division_HT_texture.SetPixel(i + l, j + m, unit_division_texture_A.GetPixel(l, m));   // 윈도우 크기만큼 허프변환 한 것을 하나씩 붙여 전체그림으로 만듬 
                    }
                }
            }
        }

        for (int i = 0; i < division_rate_for_hough; i++)
        {
            for (int j = 0; j < division_rate_for_hough; j++)
            {
                for (int k = 0; k < 360; k++)
                {
                    for (int l = 0; l < diagonal; l++)
                    {
                        if (GroupH[i, j, k, l].Count > thres_division)
                        {
                            point_count += GroupH[i, j, k, l].Count;
                            line_count++;

                            max_distance = 0;

                            for (int m = 0; m < GroupH[i, j, k, l].Count; m++)
                            {
                                for (int n = 0; n < GroupH[i, j, k, l].Count; n++)
                                {
                                    if (m != n)
                                    {
                                        dx = GroupH[i, j, k, l][n].getX() - GroupH[i, j, k, l][m].getX();
                                        dy = GroupH[i, j, k, l][n].getY() - GroupH[i, j, k, l][m].getY();
                                        if (max_distance < (float)Math.Sqrt(dx * dx + dy * dy))
                                        {
                                            first_point = GroupH[i, j, k, l][m];
                                            last_point = GroupH[i, j, k, l][n];
                                            max_distance = (float)Math.Sqrt(dx * dx + dy * dy);
                                        }
                                    }
                                }
                            }
                            Group_Hough_Line_List.Add(new Point[] {new Point(first_point.getX(), first_point.getY()),
                                    new Point(last_point.getX(), last_point.getY())});
                        }
                    }
                }
            }
        }

        print("total number of lines : " + line_count + " , " + Group_Hough_Line_List.Count);
        print("total number of points : " + point_count);

        for (int i = 0; i < Group_Hough_Line_List.Count; i++)
        {
            for (int j = i; j < Group_Hough_Line_List.Count; j++)
            {
                if (i != j)
                {
                    if (
                        (Group_Hough_Line_List[i][0].getX() == Group_Hough_Line_List[j][0].getX()) &&
                        (Group_Hough_Line_List[i][0].getY() == Group_Hough_Line_List[j][0].getY()) &&
                        (Group_Hough_Line_List[i][1].getX() == Group_Hough_Line_List[j][1].getX()) &&
                        (Group_Hough_Line_List[i][1].getY() == Group_Hough_Line_List[j][1].getY())
                    )
                    {
                        Group_Hough_Line_List.RemoveAt(j);
                        j--;
                    }
                }
            }
        }

        print("total number of lines : " + Group_Hough_Line_List.Count);
        do
        {
            Group_count = 0;
            for (int i = 0; i < Group_Hough_Line_List.Count; i++)
            {
                for (int j = i; j < Group_Hough_Line_List.Count; j++)
                {                                                                                           // 단계 1
                    if (i != j)
                    {
                        dx1 = Group_Hough_Line_List[i][1].getX() - Group_Hough_Line_List[i][0].getX();
                        dy1 = Group_Hough_Line_List[i][1].getY() - Group_Hough_Line_List[i][0].getY();
                        dx2 = Group_Hough_Line_List[j][1].getX() - Group_Hough_Line_List[j][0].getX();
                        dy2 = Group_Hough_Line_List[j][1].getY() - Group_Hough_Line_List[j][0].getY();
                        d_angle = Math.Abs(Math.Atan2(dy1, dx1) - Math.Atan2(dy2, dx2));                    // 단계 2
                        if (d_angle < 0.14)                                                              // 단계 4
                        {
                            dx = Group_Hough_Line_List[i][1].getX() - Group_Hough_Line_List[j][0].getX();
                            dy = Group_Hough_Line_List[i][1].getY() - Group_Hough_Line_List[j][0].getY();
                            distance = (float)Math.Sqrt(dx * dx + dy * dy);
                            if (distance <= critical_value)                                                             // 단계 5
                            {
                                Group_Hough_Line_List[i][1] = new Point(Group_Hough_Line_List[j][1].getX(), Group_Hough_Line_List[j][1].getY());
                                Group_Hough_Line_List.RemoveAt(j);
                                j--;
                                Group_count++;
                            }
                            else
                            {
                                s1 = Group_Hough_Line_List[i][0].getX() - Group_Hough_Line_List[i][1].getX();
                                s2 = Group_Hough_Line_List[j][0].getX() - Group_Hough_Line_List[j][1].getX();
                                r1 = Group_Hough_Line_List[i][1].getY() - Group_Hough_Line_List[i][0].getY();
                                r2 = Group_Hough_Line_List[j][1].getY() - Group_Hough_Line_List[j][0].getY();
                                t1 = Group_Hough_Line_List[i][1].getX() * Group_Hough_Line_List[i][0].getY()
                                    - Group_Hough_Line_List[i][0].getX() * Group_Hough_Line_List[i][1].getY();
                                t2 = Group_Hough_Line_List[j][1].getX() * Group_Hough_Line_List[j][0].getY()
                                    - Group_Hough_Line_List[j][0].getX() * Group_Hough_Line_List[j][1].getY();

                                if ((s2 * r1 - s1 * r2) != 0)                                               // 단계 6
                                {
                                    CrossX = (s1 * t2 - s2 * t1) / (s2 * r1 - s1 * r2);
                                    CrossY = (t1 * r2 - t2 * r1) / (s2 * r1 - s1 * r2);

                                    BoundX_Max1 = Group_Hough_Line_List[i][1].getX();
                                    BoundX_Min1 = Group_Hough_Line_List[i][0].getX();
                                    BoundX_Max2 = Group_Hough_Line_List[j][1].getX();
                                    BoundX_Min2 = Group_Hough_Line_List[j][0].getX();

                                    if (Group_Hough_Line_List[i][1].getY() >= Group_Hough_Line_List[i][0].getY())
                                    {
                                        BoundY_Max1 = Group_Hough_Line_List[i][1].getY();
                                        BoundY_Min1 = Group_Hough_Line_List[i][0].getY();
                                    }
                                    else
                                    {
                                        BoundY_Max1 = Group_Hough_Line_List[i][0].getY();
                                        BoundY_Min1 = Group_Hough_Line_List[i][1].getY();
                                    }

                                    if (Group_Hough_Line_List[j][1].getY() >= Group_Hough_Line_List[j][0].getY())
                                    {
                                        BoundY_Max2 = Group_Hough_Line_List[j][1].getY();
                                        BoundY_Min2 = Group_Hough_Line_List[j][0].getY();
                                    }
                                    else
                                    {
                                        BoundY_Max2 = Group_Hough_Line_List[j][0].getY();
                                        BoundY_Min2 = Group_Hough_Line_List[j][1].getY();
                                    }

                                    if ((CrossX >= BoundX_Min1) && (CrossX <= BoundX_Max1) &&
                                        (CrossY >= BoundY_Min1) && (CrossY <= BoundY_Max1) &&
                                        (CrossX >= BoundX_Min2) && (CrossX <= BoundX_Max2) &&
                                        (CrossY >= BoundY_Min2) && (CrossY <= BoundY_Max2)
                                    )
                                    {
                                        Group_count++;
                                        dx = Group_Hough_Line_List[i][1].getX() - Group_Hough_Line_List[i][0].getX();
                                        dy = Group_Hough_Line_List[i][1].getY() - Group_Hough_Line_List[i][0].getY();
                                        dis21 = (float)Math.Sqrt(dx * dx + dy * dy);
                                        dx = Group_Hough_Line_List[j][1].getX() - Group_Hough_Line_List[j][0].getX();
                                        dy = Group_Hough_Line_List[j][1].getY() - Group_Hough_Line_List[j][0].getY();
                                        dis43 = (float)Math.Sqrt(dx * dx + dy * dy);
                                        dx = Group_Hough_Line_List[i][1].getX() - Group_Hough_Line_List[j][0].getX();
                                        dy = Group_Hough_Line_List[i][1].getY() - Group_Hough_Line_List[j][0].getY();
                                        dis23 = (float)Math.Sqrt(dx * dx + dy * dy);
                                        dx = Group_Hough_Line_List[j][1].getX() - Group_Hough_Line_List[i][0].getX();
                                        dy = Group_Hough_Line_List[j][1].getY() - Group_Hough_Line_List[i][0].getY();
                                        dis14 = (float)Math.Sqrt(dx * dx + dy * dy);

                                        max_distance = dis21;
                                        first_point = Group_Hough_Line_List[i][0];
                                        last_point = Group_Hough_Line_List[i][1];

                                        if (dis43 > max_distance)
                                        {
                                            max_distance = dis43;
                                            first_point = Group_Hough_Line_List[j][0];
                                            last_point = Group_Hough_Line_List[j][1];
                                        }
                                        if (dis23 > max_distance)
                                        {
                                            max_distance = dis23;
                                            first_point = Group_Hough_Line_List[j][0];
                                            last_point = Group_Hough_Line_List[i][1];
                                        }
                                        if (dis14 > max_distance)
                                        {
                                            max_distance = dis14;
                                            first_point = Group_Hough_Line_List[i][0];
                                            last_point = Group_Hough_Line_List[j][1];
                                        }

                                        Group_Hough_Line_List[i][0] = new Point(first_point.getX(), first_point.getY());
                                        Group_Hough_Line_List[i][1] = new Point(last_point.getX(), last_point.getY());
                                        Group_Hough_Line_List.RemoveAt(j);
                                        j--;
                                    }
                                    else
                                    {                                                                       // 단계 7
                                        dx1 = Group_Hough_Line_List[i][1].getX() - Group_Hough_Line_List[i][0].getX();
                                        dy1 = Group_Hough_Line_List[i][1].getY() - Group_Hough_Line_List[i][0].getY();
                                        dx2 = Group_Hough_Line_List[j][1].getX() - Group_Hough_Line_List[j][0].getX();
                                        dy2 = Group_Hough_Line_List[j][1].getY() - Group_Hough_Line_List[j][0].getY();
                                        distance1 = (float)Math.Sqrt(dx1 * dx1 + dy1 * dy1);
                                        distance2 = (float)Math.Sqrt(dx2 * dx2 + dy2 * dy2);
                                        k_d1 = 1f / distance1;
                                        k_d2 = 1f / distance2;

                                        min_distance = critical_value;

                                        for (float a = 0; a < 1 + k_d1; a += k_d1)
                                        {
                                            for (float b = 0; b < 1 + k_d2; b += k_d2)
                                            {
                                                mid_X1 = Group_Hough_Line_List[i][0].getX() + (int)(a * (Group_Hough_Line_List[i][1].getX() - Group_Hough_Line_List[i][0].getX()));
                                                mid_Y1 = Group_Hough_Line_List[i][0].getY() + (int)(a * (Group_Hough_Line_List[i][1].getY() - Group_Hough_Line_List[i][0].getY()));
                                                mid_X2 = Group_Hough_Line_List[j][0].getX() + (int)(a * (Group_Hough_Line_List[j][1].getX() - Group_Hough_Line_List[j][0].getX()));
                                                mid_Y2 = Group_Hough_Line_List[j][0].getY() + (int)(a * (Group_Hough_Line_List[j][1].getY() - Group_Hough_Line_List[j][0].getY()));
                                                distance = (float)Math.Sqrt((mid_X2 - mid_X1) * (mid_X2 - mid_X1) + (mid_Y2 - mid_Y1) * (mid_Y2 - mid_Y1));

                                                if (min_distance > distance)
                                                    min_distance = distance;
                                            }
                                        }

                                        if (min_distance < critical_value)
                                        {
                                            Group_count++;
                                            dx = Group_Hough_Line_List[i][1].getX() - Group_Hough_Line_List[i][0].getX();
                                            dy = Group_Hough_Line_List[i][1].getY() - Group_Hough_Line_List[i][0].getY();
                                            dis21 = (float)Math.Sqrt(dx * dx + dy * dy);
                                            dx = Group_Hough_Line_List[j][1].getX() - Group_Hough_Line_List[j][0].getX();
                                            dy = Group_Hough_Line_List[j][1].getY() - Group_Hough_Line_List[j][0].getY();
                                            dis43 = (float)Math.Sqrt(dx * dx + dy * dy);
                                            dx = Group_Hough_Line_List[i][1].getX() - Group_Hough_Line_List[j][0].getX();
                                            dy = Group_Hough_Line_List[i][1].getY() - Group_Hough_Line_List[j][0].getY();
                                            dis23 = (float)Math.Sqrt(dx * dx + dy * dy);
                                            dx = Group_Hough_Line_List[j][1].getX() - Group_Hough_Line_List[i][0].getX();
                                            dy = Group_Hough_Line_List[j][1].getY() - Group_Hough_Line_List[i][0].getY();
                                            dis14 = (float)Math.Sqrt(dx * dx + dy * dy);

                                            max_distance = dis21;
                                            first_point = Group_Hough_Line_List[i][0];
                                            last_point = Group_Hough_Line_List[i][1];

                                            if (dis43 > max_distance)
                                            {
                                                max_distance = dis43;
                                                first_point = Group_Hough_Line_List[j][0];
                                                last_point = Group_Hough_Line_List[j][1];
                                            }
                                            if (dis23 > max_distance)
                                            {
                                                max_distance = dis23;
                                                first_point = Group_Hough_Line_List[j][0];
                                                last_point = Group_Hough_Line_List[i][1];
                                            }
                                            if (dis14 > max_distance)
                                            {
                                                max_distance = dis14;
                                                first_point = Group_Hough_Line_List[i][0];
                                                last_point = Group_Hough_Line_List[j][1];
                                            }

                                            Group_Hough_Line_List[i][0] = new Point(first_point.getX(), first_point.getY());
                                            Group_Hough_Line_List[i][1] = new Point(last_point.getX(), last_point.getY());
                                            Group_Hough_Line_List.RemoveAt(j);
                                            j--;
                                        }
                                    }
                                }
                                else
                                {                                                                           // 단계 7
                                    dx1 = Group_Hough_Line_List[i][1].getX() - Group_Hough_Line_List[i][0].getX();
                                    dy1 = Group_Hough_Line_List[i][1].getY() - Group_Hough_Line_List[i][0].getY();
                                    dx2 = Group_Hough_Line_List[j][1].getX() - Group_Hough_Line_List[j][0].getX();
                                    dy2 = Group_Hough_Line_List[j][1].getY() - Group_Hough_Line_List[j][0].getY();
                                    distance1 = (float)Math.Sqrt(dx1 * dx1 + dy1 * dy1);
                                    distance2 = (float)Math.Sqrt(dx2 * dx2 + dy2 * dy2);
                                    k_d1 = 1f / distance1;
                                    k_d2 = 1f / distance2;

                                    min_distance = critical_value;

                                    for (float a = 0; a < 1 + k_d1; a += k_d1)
                                    {
                                        for (float b = 0; b < 1 + k_d2; b += k_d2)
                                        {
                                            mid_X1 = Group_Hough_Line_List[i][0].getX() + (int)(a * (Group_Hough_Line_List[i][1].getX() - Group_Hough_Line_List[i][0].getX()));
                                            mid_Y1 = Group_Hough_Line_List[i][0].getY() + (int)(a * (Group_Hough_Line_List[i][1].getY() - Group_Hough_Line_List[i][0].getY()));
                                            mid_X2 = Group_Hough_Line_List[j][0].getX() + (int)(a * (Group_Hough_Line_List[j][1].getX() - Group_Hough_Line_List[j][0].getX()));
                                            mid_Y2 = Group_Hough_Line_List[j][0].getY() + (int)(a * (Group_Hough_Line_List[j][1].getY() - Group_Hough_Line_List[j][0].getY()));
                                            distance = (float)Math.Sqrt((mid_X2 - mid_X1) * (mid_X2 - mid_X1) + (mid_Y2 - mid_Y1) * (mid_Y2 - mid_Y1));

                                            if (min_distance > distance)
                                                min_distance = distance;
                                        }
                                    }

                                    if (min_distance < critical_value)
                                    {
                                        //print(min_distance);
                                        Group_count++;
                                        dx = Group_Hough_Line_List[i][1].getX() - Group_Hough_Line_List[i][0].getX();
                                        dy = Group_Hough_Line_List[i][1].getY() - Group_Hough_Line_List[i][0].getY();
                                        dis21 = (float)Math.Sqrt(dx * dx + dy * dy);
                                        dx = Group_Hough_Line_List[j][1].getX() - Group_Hough_Line_List[j][0].getX();
                                        dy = Group_Hough_Line_List[j][1].getY() - Group_Hough_Line_List[j][0].getY();
                                        dis43 = (float)Math.Sqrt(dx * dx + dy * dy);
                                        dx = Group_Hough_Line_List[i][1].getX() - Group_Hough_Line_List[j][0].getX();
                                        dy = Group_Hough_Line_List[i][1].getY() - Group_Hough_Line_List[j][0].getY();
                                        dis23 = (float)Math.Sqrt(dx * dx + dy * dy);
                                        dx = Group_Hough_Line_List[j][1].getX() - Group_Hough_Line_List[i][0].getX();
                                        dy = Group_Hough_Line_List[j][1].getY() - Group_Hough_Line_List[i][0].getY();
                                        dis14 = (float)Math.Sqrt(dx * dx + dy * dy);

                                        max_distance = dis21;
                                        first_point = Group_Hough_Line_List[i][0];
                                        last_point = Group_Hough_Line_List[i][1];

                                        if (dis43 > max_distance)
                                        {
                                            max_distance = dis43;
                                            first_point = Group_Hough_Line_List[j][0];
                                            last_point = Group_Hough_Line_List[j][1];
                                        }
                                        if (dis23 > max_distance)
                                        {
                                            max_distance = dis23;
                                            first_point = Group_Hough_Line_List[j][0];
                                            last_point = Group_Hough_Line_List[i][1];
                                        }
                                        if (dis14 > max_distance)
                                        {
                                            max_distance = dis14;
                                            first_point = Group_Hough_Line_List[i][0];
                                            last_point = Group_Hough_Line_List[j][1];
                                        }

                                        Group_Hough_Line_List[i][0] = new Point(first_point.getX(), first_point.getY());
                                        Group_Hough_Line_List[i][1] = new Point(last_point.getX(), last_point.getY());
                                        Group_Hough_Line_List.RemoveAt(j);
                                        j--;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            print("Group_count : " + Group_count);
        } while (Group_count != 0);

        for (int i = 0; i < Group_Hough_Line_List.Count; i++)
        {
            drawing_line_with_2dots(division_HT_texture, Group_Hough_Line_List[i][0], Group_Hough_Line_List[i][1]);

            if (Group_Hough_Line_List[i][0].getX() >= width / 2)
            {
                Hough_Line_List_Left.Add(new Point[]{
                    new Point(Group_Hough_Line_List[i][0].getX(), Group_Hough_Line_List[i][0].getY()),
                    new Point(Group_Hough_Line_List[i][1].getX(), Group_Hough_Line_List[i][1].getY())
                });
            }
            else
            {
                Hough_Line_List_Right.Add(new Point[]{
                    new Point(Group_Hough_Line_List[i][0].getX(), Group_Hough_Line_List[i][0].getY()),
                    new Point(Group_Hough_Line_List[i][1].getX(), Group_Hough_Line_List[i][1].getY())
                });
            }
        }

        print("total number of lines : " + Group_Hough_Line_List.Count);

        division_HT_texture.Apply();
    }

    void drawing_line_with_2dots(Texture2D input_texture, Point point1, Point point2)
    {
        int dis_x = point2.getX() - point1.getX();                              // 두 점 사이의 x좌표상의 거리 차
        int dis_y = point2.getY() - point1.getY();                              // 두 점 사이의 y좌표상의 거리 차

        int distance = (int)Math.Sqrt((dis_x * dis_x) + (dis_y * dis_y));       // 두 점 사이의 거리 계산 (피타고라스)

        float d_k = 1f / (float)distance;

        for (float k = 0; k < 1 + d_k; k += d_k)
        {
            input_texture.SetPixel(
                point1.getX() + (int)(k * (point2.getX() - point1.getX())),
                point1.getY() + (int)(k * (point2.getY() - point1.getY())),
                Color.blue
            );
        }
        // 첫번째 점의 좌표부터 (x 좌표는, 해당 선의 첫 점 부터 x축 방향으로 증가하며 선분의 거리만큼 점을 찍음
    }
}