using UnityEngine;
using System.Collections;

public class StartEvent : MonoBehaviour {

    public Texture[] _texture;

    private bool _onbutton;
    private bool _clickbutton;

    private float AxisXmin;
    private float AxisXmax;
    private float AxisYmin;
    private float AxisYmax;

    private float scrollSpeed;
    private float offset;
	// Use this for initialization
    void Start()
    {
        _onbutton = false;
        _clickbutton = false;
        scrollSpeed = 0.3f;
        offset = 0.0f;
    }
	
	// Update is called once per frame
	void Update ()
    {
        AxisXmin = GameObject.Find("Main Camera").camera.pixelWidth * 0.403f;
        AxisXmax = GameObject.Find("Main Camera").camera.pixelWidth * 0.597f;
        AxisYmin = GameObject.Find("Main Camera").camera.pixelHeight * 0.623f;
        AxisYmax = GameObject.Find("Main Camera").camera.pixelHeight * 0.719f;

        offset = Time.time * scrollSpeed;
        GameObject.Find("Background_Plane").renderer.material.mainTextureOffset = new Vector2(offset, 0);
	}

    void OnGUI()
    {
        Event e = Event.current;

        if ((e.mousePosition.x > AxisXmin && e.mousePosition.x < AxisXmax) && (e.mousePosition.y > AxisYmin && e.mousePosition.y < AxisYmax))
        {
            // 첫번째 (시작 버튼 영역에 마우스가 들어오면)
            GameObject.Find("Button1").renderer.material.mainTexture = _texture[1];
            if (_onbutton == false)
            {
                _onbutton = true;
                GameObject.Find("AudioSource_ButtonOn").GetComponent<AudioSource>().Play();
            }
            if (Input.GetMouseButtonDown(0))    // 마우스 왼쪽 버튼이 클릭되면
            {
                if (_clickbutton == false)
                {
                    _clickbutton = true;
                    GameObject.Find("AudioSource_ButtonClick").GetComponent<AudioSource>().Play();
                    StartCoroutine("Delay");
                }
            }
            _clickbutton = false;
        }
        else
        {
            _onbutton = false;
            // 첫번째 (시작 버튼 영역바깥에 마우스가 있으면)
            GameObject.Find("Button1").renderer.material.mainTexture = _texture[0];
        }
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(0.3f);
        Application.LoadLevel("2.CaptureImage");
        yield return null;
    }
}
