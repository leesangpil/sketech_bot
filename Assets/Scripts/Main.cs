﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;

public class Main : MonoBehaviour
{
	Texture2D capture_shot;                 // 불러올 캡쳐된 이미지
	Texture2D gray_shot;                    // 흑백 이미지
    Texture2D canny_shot;                   // 캐니 엣지 이미지
    Texture2D line_trace_shot;              // 라인 트레이서 이미지
    Texture2D gray_mosaic_shot;             // 흑백 모자이크 이미지
    Texture2D abstract_line_shot;           // 추상적 선 이미지
    Texture2D segment_trace_shot;            // 선분트래킹 이미지
    Texture2D division_hough_shot;           // 허프 변환 이미지
	
	public int width;                       // 이미지의 폭
	public int height;                      // 이미지의 높이

    public GameObject ProgramManager;               // 프로그램 매니저

    private float scrollSpeed;
    private float offset;

    float speed;
    // Use this for initialization
    void Start()
    {
        scrollSpeed = 0.087f;
        offset = 0.0f;

        speed = 0.02f;
        width = 640;        // 이미지의 폭
        height = 480;       // 이미지의 높이

        capture_shot                =    new Texture2D(width, height);              // 캡쳐
		gray_shot 				    =    new Texture2D(width, height);              // 흑백
        canny_shot                  =    new Texture2D(width, height);              // 캐니 엣지 추출
        line_trace_shot             =    new Texture2D(width, height);              // 라인 트레이서
        gray_mosaic_shot            =    new Texture2D(width, height);              // 흑백 모자이크
        abstract_line_shot          =    new Texture2D(width, height);              // 추상적 선
        segment_trace_shot          =    new Texture2D(width, height);              // 선분트래킹 이미지
        division_hough_shot         =    new Texture2D(width, height);              // 허프 변환 이미지

        ProgramManager = GameObject.Find("ProgramManager");

        // 선택한 번호 출력(그리기 방식)
        print(SelectClickEvent.selectNum);

        // 찍힌사진 불러오기
        capture_shot = SelectMode.capture_shot;
        GameObject.Find("Plane1").renderer.material.mainTexture = capture_shot;

        // 그레이 이미지 불러오기
        gray_shot = SelectMode.gray_shot;
        GameObject.Find("Plane2").renderer.material.mainTexture = gray_shot;

        // 앞에서 그림들 받아오기
        if (SelectClickEvent.selectNum == 1)
        {
            canny_shot = SelectMode.canny_shot;
            GameObject.Find("Plane3").renderer.material.mainTexture = canny_shot;
            line_trace_shot = SelectMode.line_trace_shot;
            GameObject.Find("Plane4").renderer.material.mainTexture = line_trace_shot;
            StartCoroutine(GameObject.Find("ProgramManager").GetComponent<RobotControl>().drawing_process_List_Right());
            StartCoroutine(GameObject.Find("ProgramManager").GetComponent<RobotControl>().drawing_process_List_Left());
        }
        else if (SelectClickEvent.selectNum == 2)
        {
            gray_mosaic_shot = SelectMode.gray_mosaic_shot;
            GameObject.Find("Plane3").renderer.material.mainTexture = gray_mosaic_shot;
            abstract_line_shot = SelectMode.abstract_line_shot;
            GameObject.Find("Plane4").renderer.material.mainTexture = abstract_line_shot;
            StartCoroutine(GameObject.Find("ProgramManager").GetComponent<RobotControl>().drawing_process_Array(ImageProcessing.Mosaic_Line_List));
        }
        else if (SelectClickEvent.selectNum == 3)
        {
            canny_shot = SelectMode.canny_shot;
            GameObject.Find("Plane3").renderer.material.mainTexture = canny_shot;
            segment_trace_shot = SelectMode.segment_trace_shot;
            GameObject.Find("Plane4").renderer.material.mainTexture = segment_trace_shot;
            StartCoroutine(GameObject.Find("ProgramManager").GetComponent<RobotControl>().drawing_process_Array_Right(ImageProcessing.Trace_Line_List_Right));
            StartCoroutine(GameObject.Find("ProgramManager").GetComponent<RobotControl>().drawing_process_Array_Left(ImageProcessing.Trace_Line_List_Left));
        }
        else if (SelectClickEvent.selectNum == 4)
        {
            canny_shot = SelectMode.canny_shot;
            GameObject.Find("Plane3").renderer.material.mainTexture = canny_shot;
            division_hough_shot = SelectMode.division_hough_shot;
            GameObject.Find("Plane4").renderer.material.mainTexture = division_hough_shot;
            StartCoroutine(GameObject.Find("ProgramManager").GetComponent<RobotControl>().drawing_process_Array_Right(ImageProcessing.Hough_Line_List_Right));
            StartCoroutine(GameObject.Find("ProgramManager").GetComponent<RobotControl>().drawing_process_Array_Left(ImageProcessing.Hough_Line_List_Left));
        }

        
    }

    // Update is called once per frame
    void Update()
    {
        offset = Time.time * scrollSpeed;
        GameObject.Find("film").renderer.material.mainTextureOffset = new Vector2(offset, 0);
        //if (SelectClickEvent.selectNum != 0)

        GameObject.Find("Plane1").transform.Translate(new Vector3(speed, 0, 0));
        GameObject.Find("Plane2").transform.Translate(new Vector3(speed, 0, 0));
        GameObject.Find("Plane3").transform.Translate(new Vector3(speed, 0, 0));
        GameObject.Find("Plane4").transform.Translate(new Vector3(speed, 0, 0));

        if (GameObject.Find("Plane4").transform.position.x <= -9.0f)
        {
            GameObject.Find("Plane1").transform.position = new Vector3(9.0f, 0f, -1.0f);
            GameObject.Find("Plane2").transform.position = new Vector3(13.1f, 0f, -1.0f);
            GameObject.Find("Plane3").transform.position = new Vector3(17.2f, 0f, -1.0f);
            GameObject.Find("Plane4").transform.position = new Vector3(21.3f, 0f, -1.0f);
        }

        // 그리기 시작
        //if (Input.GetKeyDown(KeyCode.Z))
        //{
        //    GameObject.Find("ProgramManager").GetComponent<RobotControl>().progress = 0;
        //StartCoroutine(GameObject.Find("ProgramManager").GetComponent<RobotControl>().drawing_process_List_Right());
        //StartCoroutine(GameObject.Find("ProgramManager").GetComponent<RobotControl>().drawing_process_List_Left());
        //}
        // 모터 초기화 (멈추면 그리던 곳부터 그림)
        if (Input.GetKey(KeyCode.S))
        {
            GameObject.Find("ProgramManager").GetComponent<RobotControl>().initialize_motors();
        }
        // 씬 넘기기
        if (Input.GetKeyDown(KeyCode.P))
        {
            SelectClickEvent.selectNum = 0;
            Application.LoadLevel("5.FinalScene");
        }
    }
}